package com.example.hrsample.controller;

import com.example.hrsample.amq.JmsProducer;
import com.example.hrsample.mapper.ConectionsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/Conections")
public class ConectionsController {
    @Autowired
    private ConectionsMapper conectionsMapper;
    @Autowired
    JmsProducer jmsProducer;

    @PostMapping("/save")
    void saveConection(Long tId, Long sId) {
        conectionsMapper.saveConection(tId, sId);
        jmsProducer.send("do do do");

    }

    @PostMapping("/delete")
    void deleteConection(Long tId, Long sId) {
        conectionsMapper.deleteConection(tId, sId);
        jmsProducer.send("do do do");
    }
}
