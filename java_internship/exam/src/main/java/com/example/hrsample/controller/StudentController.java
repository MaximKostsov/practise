package com.example.hrsample.controller;

import com.example.hrsample.amq.JmsProducer;
import com.example.hrsample.dto.StudentDTO;
import com.example.hrsample.service.api.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/student")
@Validated
public class StudentController {
    @Autowired
    private StudentService studentService;

    @Autowired
    JmsProducer jmsProducer;

    @GetMapping
    public List<StudentDTO> getAll() {
        jmsProducer.send("do do do");
        return studentService.getAll();
    }


    @PostMapping("/save")
    public void save(@RequestBody StudentDTO studentDTO) {
        studentService.save(studentDTO);
        jmsProducer.send("do do do");
    }


    @PostMapping("/delete")
    public void delete(@RequestBody StudentDTO studentDTO) {
        studentService.delete(studentDTO);
        jmsProducer.send("do do do");
    }

    @PostMapping("/update")
    public void update(@RequestBody StudentDTO studentDTO) {
        studentService.update(studentDTO);
        jmsProducer.send("do do do");
    }

    @PostMapping("/by")
    public List<StudentDTO> getAllStudentByTeacher(Long id) {
        jmsProducer.send("do do do");
        return studentService.getAllStudentByTeacher(id);

    }
}