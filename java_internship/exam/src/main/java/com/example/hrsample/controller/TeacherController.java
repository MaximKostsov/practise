package com.example.hrsample.controller;


import com.example.hrsample.amq.JmsProducer;
import com.example.hrsample.dto.TeacherDTO;
import com.example.hrsample.service.api.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/teacher")
public class TeacherController {
    @Autowired
    private TeacherService teacherService;

    @Autowired
    JmsProducer jmsProducer;


    @GetMapping
    public List<TeacherDTO> getAll() {
        jmsProducer.send("do do do");
        return teacherService.getAll();
    }

    @PostMapping("/save")
    public void save(@RequestBody TeacherDTO teacherDTO) {
        teacherService.save(teacherDTO);
        jmsProducer.send("do do do");
    }


    @PostMapping("/delete")
    public void delete(@RequestBody TeacherDTO teacherDTO) {
        teacherService.delete(teacherDTO);
        jmsProducer.send("do do do");
    }

    @PostMapping("/update")
    public void update(@RequestBody TeacherDTO teacherDTO) {
        teacherService.update(teacherDTO);
        jmsProducer.send("do do do");
    }

    @PostMapping("/by")
    public List<TeacherDTO> getAllTeachersByStudent(Long id) {
        jmsProducer.send("do do do");
        return teacherService.getAllTeachersByStudent(id);
    }
}
