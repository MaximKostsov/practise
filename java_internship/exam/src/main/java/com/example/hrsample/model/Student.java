package com.example.hrsample.model;

import lombok.Data;


@Data
public class Student {
    private Long studentId;
    private String fio;
    private String course;
    private String specialty;
}
