package com.example.hrsample.model;

import lombok.Data;
@Data
public class Teacher {
    private Long teacherId;
    private String fio;
    private String faculty;
}
