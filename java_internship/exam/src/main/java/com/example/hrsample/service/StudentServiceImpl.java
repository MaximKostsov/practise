package com.example.hrsample.service;


import com.example.hrsample.dto.StudentDTO;

import com.example.hrsample.mapper.StudentMapper;

import com.example.hrsample.model.Student;
import com.example.hrsample.service.api.StudentService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class StudentServiceImpl implements StudentService {
    @Autowired
    private StudentMapper studentMapper;

    private final ModelMapper mapper = new ModelMapper();

    @Override
    public void save(StudentDTO studentDTO) {
        Student student = mapper.map(studentDTO, Student.class);
        studentMapper.save(student);
    }

    @Override
    public List<StudentDTO> getAllStudentByTeacher(Long id) {
        return studentMapper.getAllStudentByTeacher(id).stream()
                .map(student -> mapper.map(student, StudentDTO.class))
                .collect(Collectors.toList());
    }


    @Override
    public List<StudentDTO> getAll() {
        return studentMapper.getAll().stream()
                .map(student -> mapper.map(student, StudentDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public void delete(StudentDTO studentDTO) {
        Student student = mapper.map(studentDTO, Student.class);
        studentMapper.delete(student);
    }

    @Override
    public void update(StudentDTO studentDTO) {
        Student student = mapper.map(studentDTO, Student.class);
        studentMapper.update(student);
    }
}
