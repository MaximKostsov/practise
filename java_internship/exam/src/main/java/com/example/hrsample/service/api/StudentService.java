package com.example.hrsample.service.api;

import com.example.hrsample.dto.StudentDTO;
import com.example.hrsample.model.Student;

import java.util.List;

public interface StudentService {
        void save(StudentDTO studentDTO);
        List<StudentDTO> getAll();
        void delete(StudentDTO studentDTO);
        void update(StudentDTO studentDTO);
        List<StudentDTO> getAllStudentByTeacher(Long id);
}
