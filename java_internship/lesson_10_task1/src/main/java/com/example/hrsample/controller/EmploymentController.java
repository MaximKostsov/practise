package com.example.hrsample.controller;


import com.example.hrsample.dto.EmploymentDTO;
import com.example.hrsample.model.Employment;
import com.example.hrsample.service.api.EmploymentService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@RestController
@RequestMapping("/api/employment")
public class EmploymentController {
    @Autowired
    private EmploymentService employmentService;

    @GetMapping
    public List<EmploymentDTO> getAll() {
        return employmentService.getAll();
    }

    @PostMapping("/save")
    public void save(@RequestBody EmploymentDTO employmentDTO) {
        employmentService.save(employmentDTO);
    }

    @PostMapping("/records/update")
    public void recordsUpdate(@RequestBody List<EmploymentDTO> employmentDTOList) {
        employmentService.recordsUpdate(employmentDTOList);
    }


    @PostMapping("/delete")
    public void delete(@RequestBody EmploymentDTO employmentDTO) {
        employmentService.delete(employmentDTO);
    }

    @PostMapping("/update")
    public void update(@RequestBody EmploymentDTO employmentDTO) {
        employmentService.update(employmentDTO);
    }
}