package com.example.hrsample.controller;


import com.example.hrsample.dto.PersonDTO;
import com.example.hrsample.model.Person;
import com.example.hrsample.service.api.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/person")
public class PersonController {
    @Autowired
    private PersonService departmentsService;

    @GetMapping
    public List<Person> getAll() {
        return departmentsService.getAll();
    }

    @PostMapping("/save")
    public void save(@RequestBody PersonDTO personDTO) {
        departmentsService.save(personDTO);
    }
}
