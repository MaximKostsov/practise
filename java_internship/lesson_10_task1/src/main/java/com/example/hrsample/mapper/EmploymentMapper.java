package com.example.hrsample.mapper;


import com.example.hrsample.model.Employment;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface EmploymentMapper {
    @Insert("insert into employment (version, start_dt, end_dt, work_type_id, organization_name, organization_address, position_name, person_id) " +
            "values (#{e.version}, #{e.startDt}, #{e.endDt}, #{e.workTypeId}, #{e.organizationName}, #{e.organizationAddress}, #{e.positionName}, #{e.personId})")
    void save(@Param("e") Employment employment);

    @Select("select * from employment")
    List<Employment> getAll();

    @Delete("delete from employment where employment_id = #{employmentId}")
    void delete(Employment employment);

    @Delete("delete from employment where employment_id = #{id}")
    void deleteById(Long id);

    @Update("update employment set version = #{e.version}, start_dt =  #{e.startDt}, end_dt = #{e.endDt}, work_type_id = #{e.workTypeId}, organization_name =#{e.organizationName}, organization_address =#{e.organizationAddress}, position_name =#{e.positionName}, person_id =#{e.personId} where employment_id = #{e.employmentId}")
    void update(@Param("e") Employment employment);
}
