package com.example.hrsample.mapper;


import com.example.hrsample.model.Person;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface PersonMapper {
    @Insert("INSERT into person(first_name, last_name, middle_name, birth_date, gender) " +
            "VALUES(#{p.firstName},#{p.lastName},#{p.middleName},#{p.birthDate},#{p.gender})")
    void save(@Param("p") Person person);

    @Select("select * from person")
    List<Person> getAll();

}
