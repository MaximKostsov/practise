package com.example.hrsample.model;

import lombok.Data;

import java.time.LocalDate;

@Data
public class Employment {
    private Long employmentId;
    private int version;
    private LocalDate startDt;
    private LocalDate endDt;
    private Long workTypeId;
    private String organizationName;
    private String organizationAddress;
    private String positionName;
    private Long personId;
}
