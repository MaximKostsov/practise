package com.example.hrsample.model;


import lombok.Data;

import java.time.LocalDate;

@Data
public class Person {
    private Long personId;
    private String firstName;
    private String lastName;
    private String middleName;
    private LocalDate birthDate;
    private String gender;
}
