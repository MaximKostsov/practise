package com.example.hrsample.service;


import com.example.hrsample.dto.EmploymentDTO;
import com.example.hrsample.mapper.EmploymentMapper;
import com.example.hrsample.model.Employment;
import com.example.hrsample.service.api.EmploymentService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;

@Service
public class EmploymentServiceImpl implements EmploymentService {
    @Autowired
    private EmploymentMapper employmentMapper;

    private final ModelMapper mapper = new ModelMapper();

    @Override
    public void save(EmploymentDTO employmentDTO) {
        Employment employment = mapper.map(employmentDTO, Employment.class);
        employmentMapper.save(employment);
    }

    @Override
    public List<EmploymentDTO> getAll() {
        return employmentMapper.getAll().stream()
                .map(employment -> mapper.map(employment, EmploymentDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public void delete(EmploymentDTO employmentDTO) {
        Employment employment = mapper.map(employmentDTO, Employment.class);
        employmentMapper.delete(employment);
    }

    @Override
    public void update(EmploymentDTO employmentDTO) {
        Employment employment = mapper.map(employmentDTO, Employment.class);
        employmentMapper.update(employment);
    }

    @Override
    public void recordsUpdate(List<EmploymentDTO> dtos) {
        for (EmploymentDTO dto : dtos) {
            if (isNull(dto.getEmploymentId())) {
                Employment employment = mapper.map(dto, Employment.class);
                employmentMapper.save(employment);
                continue;
            }
            if (isNull(dto.getVersion())
                    && isNull(dto.getEndDt())
                    && isNull(dto.getStartDt())
                    && isNull(dto.getOrganizationAddress())
                    && isNull(dto.getOrganizationName())
                    && isNull(dto.getWorkTypeId())
                    && isNull(dto.getVersion())
                    && isNull(dto.getPositionName())
                    && isNull(dto.getPersonId())) {
                employmentMapper.deleteById(dto.getEmploymentId());
            } else {
                Employment employment = mapper.map(dto, Employment.class);
                employmentMapper.update(employment);
            }
        }
    }
}
