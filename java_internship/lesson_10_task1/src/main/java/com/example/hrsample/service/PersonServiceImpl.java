package com.example.hrsample.service;


import com.example.hrsample.dto.PersonDTO;
import com.example.hrsample.mapper.PersonMapper;
import com.example.hrsample.model.Person;
import com.example.hrsample.service.api.PersonService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonServiceImpl implements PersonService {
    @Autowired
    private PersonMapper personMapper;

    private final ModelMapper mapper = new ModelMapper();

    @Override
    public void save(PersonDTO personDTO) {
        Person person = mapper.map(personDTO, Person.class);
        personMapper.save(person);
    }

    @Override
    public List<Person> getAll() {
        return personMapper.getAll();
    }
}
