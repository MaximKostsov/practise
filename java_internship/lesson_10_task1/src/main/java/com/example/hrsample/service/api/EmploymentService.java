package com.example.hrsample.service.api;


import com.example.hrsample.dto.EmploymentDTO;
import com.example.hrsample.model.Employment;

import java.util.List;

public interface EmploymentService {
    void save(EmploymentDTO employmentDTO);
    List<EmploymentDTO> getAll();
    void delete(EmploymentDTO employmentDTO);
    void update(EmploymentDTO employmentDTO);
    void recordsUpdate(List<EmploymentDTO> dtos);

}
