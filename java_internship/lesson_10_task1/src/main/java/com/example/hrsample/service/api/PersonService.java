package com.example.hrsample.service.api;



import com.example.hrsample.dto.PersonDTO;
import com.example.hrsample.model.Person;

import java.util.List;

public interface PersonService {
    void save(PersonDTO personDTO);
    List<Person> getAll();
}
