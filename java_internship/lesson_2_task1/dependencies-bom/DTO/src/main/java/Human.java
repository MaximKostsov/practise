import java.util.Objects;

public class Human {
    public String name;
    public String secondName;
    public int age;
    public String gender;
    public String dirthday;

    public Human(String name, String secondName, int age, String gender, String dirthdey) {
        this.name = name;
        this.secondName = secondName;
        this.age = age;
        this.gender = gender;
        this.dirthday = dirthdey;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public boolean coincidence(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return age == human.age ||
                Objects.equals(name, human.name) ||
                Objects.equals(secondName, human.secondName) ||
                Objects.equals(dirthday, human.dirthday) ||
                Objects.equals(gender, human.gender);
    }

    public String getDirthdey() {
        return dirthday;
    }

    public void setDirthdey(String dirthdey) {
        this.dirthday = dirthdey;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return age == human.age &&
                Objects.equals(name, human.name) &&
                Objects.equals(secondName, human.secondName) &&
                Objects.equals(dirthday, human.dirthday) &&
                Objects.equals(gender, human.gender);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, secondName, age, gender);
    }

    @Override
    public String toString() {
        return
                "{name='" + name + '\'' +
                ", secondName='" + secondName + '\'' +
                ", age=" + age +
                ", gender='" + gender + '\'' +
                '}';
    }
}