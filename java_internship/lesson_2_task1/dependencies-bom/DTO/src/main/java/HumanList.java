import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class HumanList {
    public static List<Human> getHumanList() {
        List<Human> humans = new ArrayList<>();
        humans.add(new Human("Vasa", "Petrov", 12, "M","2 июня 1996 года" ));
        humans.add(new Human("Jora", "Vetrov", 24, "M", "13 июня 1994 года"));
        humans.add(new Human("Veta", "Borod", 32, "F", "24 июня 1998 года"));
        humans.add(new Human("Masha", "Ivanova", 32, "M","1 июня 1995 года" ));
        humans.add(new Human("Masha", "Ivanova", 32, "M","1 июня 1995 года" ));
        humans.add(new Human("Masha", "Ivanova", 32, "M","1 июня 1995 года" ));
        humans.add(new Human("Masha", "Ivanova", 32, "M","1 июня 1995 года" ));
        humans.add(new Human("Grisha", "Antonov", 24, "M","7 июня 1993 года"));
        humans.add(new Human("Petia", "Camar", 24, "M", "8 июня 2000 года"));
        humans.add(new Human("Petia", "Camar", 24, "M", "8 июня 2000 года"));
        humans.add(new Human("Petia", "Camar", 24, "M", "8 июня 2000 года"));
        humans.add(new Human("Petia", "Camar", 24, "M", "8 июня 2000 года"));
        return humans;
    }
}
