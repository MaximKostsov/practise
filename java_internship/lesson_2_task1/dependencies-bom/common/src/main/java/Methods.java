import java.util.*;
import java.util.stream.Collectors;

public class Methods {
    static Integer count = 1;

    public static void firstTask(List<Human> humans, Human humanTest) {
        System.out.println(humans.stream().anyMatch(humanTest::coincidence) ? "Совпадение есть !" : "Совпадений нет !");
    }

    public static void secondTask(List<Human> humans) {
        humans.stream()
                .filter(human -> human.getAge() > 22 && human.getSecondName().matches("^[A-Ba-b][a-z]{1,23}$"))
                .forEach(System.out::println);
    }

    public static void firdTask(List<Human> humans) {
        count = 1;
        System.out.println("Повторяющиеся люди :");
        List<Human> repeatingObjectsTwo =
                humans.stream()
                        .sorted(Comparator.comparing(Human::getAge).reversed())
                        .filter(h -> {
                                    if (Collections.frequency(humans, h) > 3) {
                                        listCount(h);
                                        return true;

                                    } else if (Collections.frequency(humans, h) > 1) {
                                        listCount(h);
                                        return false;
                                    }
                                    return false;
                                }
                        ).collect(Collectors.toList());
        System.out.println("Список дублей людей с повторами больше 3:");
        repeatingObjectsTwo.forEach(System.out::println);
        System.out.println("Множество людей с повторами");
        humanSet(repeatingObjectsTwo);

    }

//    public static void fourthTask(List<Human> humans, Human humanTest) {
//        long countHuman = humans.stream().filter(humanTest::coincidence).count();
//        count = 1;
//        List<Human> repeatingObjectsOne = humans.stream()
//                .filter(h -> {
//                    if (humanTest.coincidence(h) && countHuman > 3) {
//                        listCount(h);
//                        return true;
//                    } else if (humanTest.coincidence(h)) {
//                        listCount(h);
//                        return false;
//                    }
//                    return false;
//                }).sorted(Comparator.comparing(Human::getAge).reversed())
//                .collect(Collectors.toList());
//        repeatingObjectsOne.forEach(System.out::println);
//    }


    private static void listCount(Human h) {
        System.out.println(count + " : " + h);
        count++;
    }

    private static void humanSet(List<Human> list) {
        count = 1;
        Set<Human> humanSet = new HashSet<>(list);
        humanSet.stream().sorted(Comparator.comparing(Human::getAge).reversed()).forEach(Methods::listCount);
    }
}
