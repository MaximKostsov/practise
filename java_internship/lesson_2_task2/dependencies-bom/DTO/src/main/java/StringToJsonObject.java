import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.text.DecimalFormat;

public class StringToJsonObject {

    public static JSONObject getJsonString(String stringJson, int money) throws ParseException {
        JSONParser parser = new JSONParser();
        JSONObject json = (JSONObject) parser.parse(stringJson);
        JSONObject moneyArray = (JSONObject) json.get("rates");
        DecimalFormat decimalFormat = new DecimalFormat("#.##");
        System.out.println("Валюта " + json.get("base"));
        System.out.println("Имеется " + money + " " + json.get("base"));
        System.out.println("Дата " + json.get("date"));
        System.out.println("Курс обмена:");
        for (Object key : moneyArray.keySet()) {
            JSONArray moneyArrayName = (JSONArray) json.get(key);
            System.out.println("Валюта " + key + " Курс " + moneyArray.get(key)
                    + " получим " + decimalFormat.format(((double) moneyArray.get(key)) * money));
        }

        return json;
    }


}
