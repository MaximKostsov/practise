package org.example.lsp;

public class Animal implements FeedTheOffspring, addEnergy {
    private int weight;
    public String name;

    public Animal(String name){
        this.name = name;
    }

    public void eating(){ //Open-Closed Principle
        if (this.name.equals("Pushok")){
            System.out.println("Pushok eating Fish");
        }
        if (this.name.equals("Aza")){
            System.out.println("Aza eating Grass");
        }
    }

    public void legs(){////Liskov Substitution Principle
        if(this instanceof Cat){
            System.out.println("Paws");
        }
        if(this instanceof Cow){
            System.out.println("Hooves");
        }
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public void catSound(Animal animal) { //Dependency Inversion Principle and KISS
        ((Cat)animal).meow();
    }



    @Override
    public void feedCalv() {

    }

    @Override
    public void feedKittens() {

    }

    @Override
    public void feedPuppies() {

    }
// Dependency Inversion Principle
    @Override
    public void eat() {

    }

    @Override
    public void refuel() {

    }

    @Override
    public void solarEnergy() {

    }
    //=============================
}
