package org.example.lsp;

public class Cow extends Animal {
    public Cow(String name) {
        super(name);
    }

    public void moo(){
        System.out.println("Mooo");
    }
}
