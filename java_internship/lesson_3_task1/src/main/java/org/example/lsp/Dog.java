package org.example.lsp;

public class Dog extends Animal {
    public Dog(String name) {
        super(name);
    }

    public void bark(){
        System.out.println("Bark");
    }


    @Override
    public void feedKittens() {
        System.out.println("Kittens");
    }

    @Override
    public void feedPuppies() {
        System.out.println("Puppies");
    }
}
