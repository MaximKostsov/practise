package org.example.lsp;

public interface FeedTheOffspring {//Interface Segregation Principle
    void feedCalv();
    void feedKittens();
    void feedPuppies();
}
