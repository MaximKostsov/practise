package org.example.lsp;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

/**
 * Hello world!
 */
public class Main {
    public static void main(String[] args) {

        Animal cat = new Cat("Pushok");
        Animal dog = new Dog("Rex");
        Animal cow = new Cow(setNameCow());
        Speaker speaker = new Speaker();
        //Single Responsibility Principle
        speaker.eat(dog);
        //==============================
        //Open-Closed Principle
        cat.eating();
        cow.eating();
        //===============================
        //Liskov Substitution Principle
        cat.legs();
        cow.legs();
        //===============================
        //Interface Segregation Principle if there is no class Animal
        dog.feedKittens();
        //YAGNI
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        System.out.println(dateFormat.format(date));
        //===============================
        //Dependency Inversion Principle and DRY maybe KISS
        cat.catSound(cat);
        //===============================
        //Dependency Inversion Principle
        cat.refuel();
        //===============================================
    }
    //KISS
    public static String setNameCow(){
        Scanner in = new Scanner(System.in);
        System.out.print("First char: ");
        String num1 =  in.nextLine();
        System.out.print("First char: ");
        String num2 =  in.nextLine();
        System.out.print("First char: ");
        String num3 =  in.nextLine();
        return num1 + num2 + num3;
    }
    //===============================
}

