package lesso.org.data.jpa.domain;

import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Stuff implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private String fio;

    @Column(nullable = false)
    private String position;



    @ManyToOne(optional = false)
    @NaturalId
    private Hotel hotel;

    public Stuff(String fio, String position) {
        this.fio = fio;
        this.position = position;
    }

    public Stuff() {

    }

    public String getFio() {
        return fio;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }
}
