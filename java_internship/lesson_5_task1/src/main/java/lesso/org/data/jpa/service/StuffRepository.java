package lesso.org.data.jpa.service;

import lesso.org.data.jpa.domain.Hotel;
import lesso.org.data.jpa.domain.RatingCount;
import lesso.org.data.jpa.domain.Stuff;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import java.util.List;

public interface StuffRepository extends Repository<Stuff, Long> {


    Stuff findByFio(String fio);

    List<Stuff> findAllByHotel(Hotel hotel);
}
