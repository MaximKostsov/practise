package lesso.org.data.jpa.service;

import lesso.org.data.jpa.domain.City;
import lesso.org.data.jpa.domain.Hotel;
import lesso.org.data.jpa.domain.HotelSummary;
import lesso.org.data.jpa.domain.Stuff;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface StuffService {
    Stuff getStuff(String fio);

    List<Stuff> findAllByHotel(Hotel hotel);
}
