package lesso.org.data.jpa.service;

import lesso.org.data.jpa.domain.Hotel;
import lesso.org.data.jpa.domain.Stuff;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.List;

@Component("stuffService")
@Transactional
public class StuffServicelmpl implements StuffService {
    private final StuffRepository stuffRepository;
    @Autowired
    public StuffServicelmpl(StuffRepository stuffRepository) {
        this.stuffRepository = stuffRepository;
    }

    @Override
    public Stuff getStuff(String fio) {
        Assert.notNull(fio, "Name must not be null");
        return this.stuffRepository.findByFio(fio);
    }

    @Override
    public List<Stuff> findAllByHotel(Hotel hotel) {
        Assert.notNull(hotel, "Name must not be null");
        return this.stuffRepository.findAllByHotel(hotel);
    }
}