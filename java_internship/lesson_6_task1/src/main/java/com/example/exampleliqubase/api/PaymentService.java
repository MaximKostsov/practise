package com.example.exampleliqubase.api;

import com.example.exampleliqubase.dto.PaymentDTO;

import java.util.List;

public interface PaymentService {
    PaymentDTO savePayment(PaymentDTO paymentDTO);

    List<PaymentDTO> getListPayments();

}
