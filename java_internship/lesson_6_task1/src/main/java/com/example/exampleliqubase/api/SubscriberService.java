package com.example.exampleliqubase.api;


import com.example.exampleliqubase.dto.SubscriberDTO;

import java.util.List;

public interface SubscriberService {
    SubscriberDTO saveSubscriber(SubscriberDTO subscriberDTO);

    List<SubscriberDTO> getListSubscribers();
}
