package com.example.exampleliqubase.api;

import com.example.exampleliqubase.dto.TariffDTO;

import java.util.List;

public interface TariffService {
    TariffDTO saveTariff(TariffDTO tariffDTO);

    List<TariffDTO> getListTariffs();
}
