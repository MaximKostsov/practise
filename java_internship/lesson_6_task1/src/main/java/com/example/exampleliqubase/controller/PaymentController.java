package com.example.exampleliqubase.controller;

import com.example.exampleliqubase.api.AccountService;
import com.example.exampleliqubase.api.PaymentService;
import com.example.exampleliqubase.dto.AccountDTO;
import com.example.exampleliqubase.dto.PaymentDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


import com.example.exampleliqubase.api.AccountService;
import com.example.exampleliqubase.dto.AccountDTO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/payment")
public class PaymentController {

    @Autowired
    private PaymentService paymentService;

    @PostMapping("/savePayment")
    public PaymentDTO savePayment(@RequestBody @Validated  PaymentDTO paymentDTO) {
        return paymentService.savePayment(paymentDTO);
    }

    @GetMapping
    public List<PaymentDTO> getListPayments() {
        return paymentService.getListPayments();
    }
}