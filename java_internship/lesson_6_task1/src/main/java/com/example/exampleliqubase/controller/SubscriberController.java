package com.example.exampleliqubase.controller;

import com.example.exampleliqubase.api.AccountService;
import com.example.exampleliqubase.api.SubscriberService;
import com.example.exampleliqubase.dto.AccountDTO;
import com.example.exampleliqubase.dto.SubscriberDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/subscribers")
public class SubscriberController {
    @Autowired
    private SubscriberService subscriberService;

    @PostMapping("/subscribersSave")
    public SubscriberDTO saveSubscriber(@RequestBody @Validated SubscriberDTO subscriberDTO) {
        return subscriberService.saveSubscriber(subscriberDTO);
    }

    @GetMapping
    public List<SubscriberDTO> getListSubscribers() {
        return subscriberService.getListSubscribers();
    }
}
