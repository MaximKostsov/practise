package com.example.exampleliqubase.controller;

import com.example.exampleliqubase.api.AccountService;
import com.example.exampleliqubase.api.TariffService;
import com.example.exampleliqubase.dto.AccountDTO;
import com.example.exampleliqubase.dto.TariffDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@RequestMapping("/tariff")
public class TariffController {
    @Autowired
    private TariffService tariffService;

    @PostMapping("/tariffSave")
    public TariffDTO saveTariff(@RequestBody @Validated TariffDTO tariffDTO) {
        return tariffService.saveTariff(tariffDTO);
    }

    @GetMapping
    public List<TariffDTO> getListAccounts() {
        return tariffService.getListTariffs();
    }
}
