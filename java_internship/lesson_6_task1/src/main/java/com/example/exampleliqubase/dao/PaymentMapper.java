package com.example.exampleliqubase.dao;

import com.example.exampleliqubase.model.PaymentEntity;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.springframework.stereotype.Repository;

import java.util.Arrays;
import java.util.List;

@Mapper
@Repository
public interface PaymentMapper {

    @SelectKey(resultType = Long.class, keyProperty = "id", before = true,
            statement = "select nextval('payment_seq')")
    @Insert("insert into payments (id, ref_subscriber, summa) " +
            "values (#{id}, #{ref_subscriber} ,#{summa} )")
    void  savePayment(PaymentEntity payment);

    @Select("select * from payments where id=#{id}")
    PaymentEntity getPaymentByIdSub(Integer id);

    @Select("select * from payments")
    List<PaymentEntity> getListPayments();
}
