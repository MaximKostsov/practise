package com.example.exampleliqubase.dao;

import com.example.exampleliqubase.model.SubscriberEntity;
import com.example.exampleliqubase.model.TariffEntity;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface SubscriberMapper {

    @SelectKey(resultType = Long.class, keyProperty = "id", before = true,
            statement = "select nextval('subscriber_seq')")
    @Insert("insert into subscriber (id, name, ref_tariff) " +
            "values (#{id}, #{name} ,#{ref_tariff} )")
    void saveSubscriber(SubscriberEntity subscriber);

    @Select("select * from subscriber where id=#{id}")
    SubscriberEntity getSubscriberById(Integer id);

    @Select("select * from subscriber")
    List<SubscriberEntity> getSubscribers();

}
