package com.example.exampleliqubase.dao;

import com.example.exampleliqubase.model.TariffEntity;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface TariffMapper {
    @Select("select * from tariff where id=#{id}")
    TariffEntity getTariffById(Integer id);

    @Select("select * from tariff")
    List<TariffEntity> getListTariffs();

    @Insert("insert into tariff(descr) values (#{descr} )")
    void saveTariff(TariffEntity tariffEntity);
}
