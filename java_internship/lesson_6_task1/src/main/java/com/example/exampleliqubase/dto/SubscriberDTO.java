package com.example.exampleliqubase.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SubscriberDTO {
    private Long id;

    @NotNull
    private String name;

    @NotNull
    private Long ref_tariff;
}