package com.example.exampleliqubase.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Connection;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class SubscriberEntity {
    private Long id;
    private String name;
    private TariffEntity tariff;
    private List<PaymentEntity> paymentEntities;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TariffEntity getTariff() {
        return tariff;
    }

    public void setTariff(TariffEntity tariff) {
        this.tariff = tariff;
    }

    public List<PaymentEntity> getPayments() {
        return paymentEntities;
    }

    public void setPayments(List<PaymentEntity> payments) {
        this.paymentEntities = payments;
    }

}
