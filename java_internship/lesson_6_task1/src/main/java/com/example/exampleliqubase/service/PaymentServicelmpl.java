package com.example.exampleliqubase.service;

import com.example.exampleliqubase.api.PaymentService;
import com.example.exampleliqubase.dao.PaymentMapper;
import com.example.exampleliqubase.dto.PaymentDTO;
import com.example.exampleliqubase.model.PaymentEntity;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PaymentServicelmpl implements PaymentService {

    @Autowired
    private PaymentMapper paymentMapper;

    @Autowired
    private ModelMapper modelMapper;

    @PostConstruct
    public void init() {
        System.out.println();
    }


    @Override
    @Transactional
    public PaymentDTO savePayment(PaymentDTO paymentDTO) {

        PaymentEntity payment = modelMapper.map(paymentDTO, PaymentEntity.class);
        paymentMapper.savePayment(payment);
        return modelMapper.map(payment, PaymentDTO.class);
    }

    @Override
    public List<PaymentDTO> getListPayments() {
        return paymentMapper.getListPayments().stream()
                .map(payment -> modelMapper.map(payment, PaymentDTO.class))
                .collect(Collectors.toList());
    }

}