package com.example.exampleliqubase.service;

import com.example.exampleliqubase.api.SubscriberService;
import com.example.exampleliqubase.dao.SubscriberMapper;
import com.example.exampleliqubase.dto.SubscriberDTO;
import com.example.exampleliqubase.model.SubscriberEntity;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.util.List;
import java.util.stream.Collectors;


    @Service
    public class SubscriberServicelmpl implements SubscriberService {

        @Autowired
        private SubscriberMapper subscriberMapper;

        @Autowired
        private ModelMapper modelMapper;

        @Autowired
        private DataSource dataSource;

        @PostConstruct
        public void init() {
            System.out.println();
        }

        @Override
        @Transactional
        public SubscriberDTO saveSubscriber(SubscriberDTO subscriberDTO) {
            SubscriberEntity subscriber = modelMapper.map(subscriberDTO, SubscriberEntity.class);
            subscriberMapper.saveSubscriber(subscriber);
            return modelMapper.map(subscriber, SubscriberDTO.class);
        }

        @Override
        public List<SubscriberDTO> getListSubscribers() {
            return subscriberMapper.getSubscribers().stream()
                    .map(subscriber -> modelMapper.map(subscriber, SubscriberDTO.class))
                    .collect(Collectors.toList());
        }
    }

