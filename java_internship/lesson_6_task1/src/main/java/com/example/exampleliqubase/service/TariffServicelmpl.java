package com.example.exampleliqubase.service;


import com.example.exampleliqubase.api.TariffService;
import com.example.exampleliqubase.dao.TariffMapper;
import com.example.exampleliqubase.dto.TariffDTO;
import com.example.exampleliqubase.model.TariffEntity;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TariffServicelmpl implements TariffService {

    @Autowired
    private TariffMapper tariffMapper;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private DataSource dataSource;

    @PostConstruct
    public void init() {
        System.out.println();
    }

    @Override
    public TariffDTO saveTariff(TariffDTO tariffDTO) {
        TariffEntity tariffEntity = modelMapper.map(tariffDTO, TariffEntity.class);
        tariffMapper.saveTariff(tariffEntity);
        return modelMapper.map(tariffEntity, TariffDTO.class);
    }

    @Override
    public List<TariffDTO> getListTariffs() {
        return tariffMapper.getListTariffs().stream()
                .map(tariff -> modelMapper.map(tariff, TariffDTO.class))
                .collect(Collectors.toList());
    }
}

