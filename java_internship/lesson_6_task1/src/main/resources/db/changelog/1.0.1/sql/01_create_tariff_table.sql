CREATE TABLE if not exists tariff
(
    id    SERIAL       NOT NULL ,
    descr VARCHAR(255) NOT NULL,
    PRIMARY KEY (id)
)