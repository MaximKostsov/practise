CREATE TABLE if not exists subscriber
(
    id         SERIAL       NOT NULL,
    name       VARCHAR(255) NOT NULL,
    ref_tariff INT          NOT NULL REFERENCES tariff (id),
    PRIMARY KEY(id)
)

