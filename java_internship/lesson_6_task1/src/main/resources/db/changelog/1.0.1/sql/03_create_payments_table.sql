CREATE TABLE if not exists payments
(
    id             SERIAL NOT NULL,
    ref_subscriber INT    NOT NULL REFERENCES subscriber (id),
    summa          INT    NOT NULL,
    PRIMARY KEY (id)
)
