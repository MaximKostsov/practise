alter table account_entity_liquibase add column addition VARCHAR(255);
update account_entity_liquibase set addition = 'notnull' where account_entity_liquibase.addition is null;
alter table account_entity_liquibase alter column addition set not null ;
