package com.example.hrsample.DTO;

import com.example.hrsample.model.Locations;
import lombok.Data;

@Data
public class DepartmentsSaveDTO {
    private String departmentName;
    private Integer managerId;
    private Integer locationId;
}
