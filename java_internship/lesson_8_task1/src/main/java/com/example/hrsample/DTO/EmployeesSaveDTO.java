package com.example.hrsample.DTO;

import com.example.hrsample.model.Departments;
import com.example.hrsample.model.Jobs;
import lombok.Data;

import java.sql.Date;
@Data
public class EmployeesSaveDTO {
    private String firstName;
    private String lastName;
    private String email;
    private String phoneNumber;
    private Date hireDate;
    private Jobs job;
    private Integer salary;
    private Integer managerId;
    private Departments department;
    private Integer commissionPct;
}
