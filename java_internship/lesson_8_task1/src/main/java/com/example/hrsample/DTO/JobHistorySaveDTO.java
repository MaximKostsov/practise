package com.example.hrsample.DTO;

import com.example.hrsample.model.Departments;
import com.example.hrsample.model.Employees;
import com.example.hrsample.model.Jobs;
import lombok.Data;

import java.sql.Date;


@Data
public class JobHistorySaveDTO {
    private Employees employee;
    private Date startDate;
    private Date endDate;
    private Jobs job;
    private Departments department;
}
