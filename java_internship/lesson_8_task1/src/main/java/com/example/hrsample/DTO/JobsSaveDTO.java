package com.example.hrsample.DTO;

import lombok.Data;

@Data
public class JobsSaveDTO {
    private String jobId;
    private String jobTitle;
    private Integer minSalary;
    private Integer maxSalary;
}
