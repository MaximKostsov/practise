package com.example.hrsample.DTO;

import lombok.Data;

@Data
public class LocationSaveDTO {
    private String streetAddress;
    private String city;
}
