package com.example.hrsample.controller;

import com.example.hrsample.DTO.DepartmentsSaveDTO;
import com.example.hrsample.DTO.LocationSaveDTO;
import com.example.hrsample.model.Departments;
import com.example.hrsample.model.Locations;
import com.example.hrsample.service.api.DepartmentsService;
import com.example.hrsample.service.api.LocationsService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/departments")
public class DepartmentsController {

    @Autowired
    private DepartmentsService departmentsService;


    @GetMapping
    public List<Departments> getAllDepartments() {
        return departmentsService.getAllDepartments();
    }


}

