package com.example.hrsample.controller;

import com.example.hrsample.DTO.LocationSaveDTO;
import com.example.hrsample.model.Employees;
import com.example.hrsample.model.Locations;
import com.example.hrsample.service.api.EmployeesService;
import com.example.hrsample.service.api.LocationsService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/employees")
public class EmployeesController {


    @Autowired
    private EmployeesService employeesService;


    @GetMapping
    public List<Employees> getAllEmployees() {
        return employeesService.getAllEmployees();
    }
}