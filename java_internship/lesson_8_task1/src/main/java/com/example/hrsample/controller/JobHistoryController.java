package com.example.hrsample.controller;
import com.example.hrsample.DTO.LocationSaveDTO;
import com.example.hrsample.model.JobHistory;
import com.example.hrsample.model.Locations;
import com.example.hrsample.service.api.JobHistoryService;
import com.example.hrsample.service.api.LocationsService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@RequestMapping("/job-history")
public class JobHistoryController {
    @Autowired
    private JobHistoryService jobHistoryService;

    @GetMapping
    public List<JobHistory> getAll() {
        return jobHistoryService.getAllJobHistory();
    }
}
