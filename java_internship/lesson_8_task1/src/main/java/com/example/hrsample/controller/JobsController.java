package com.example.hrsample.controller;
import com.example.hrsample.DTO.JobsSaveDTO;
import com.example.hrsample.DTO.LocationSaveDTO;
import com.example.hrsample.model.Jobs;
import com.example.hrsample.model.Locations;
import com.example.hrsample.service.api.JobsService;
import com.example.hrsample.service.api.LocationsService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@RequestMapping("/jobs")
public class JobsController {

    @Autowired
    private JobsService jobsService;


    @GetMapping
    public List<Jobs> getAllJobs() {
        return jobsService.getAllJobs();
    }
}

