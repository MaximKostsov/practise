package com.example.hrsample.controller;

import com.example.hrsample.DTO.LocationSaveDTO;
import com.example.hrsample.model.Locations;
import com.example.hrsample.service.api.LocationsService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/locations")
public class LocationsController {

    @Autowired
    private LocationsService locationsService;

    @GetMapping
    public List<Locations> getAll(){
        return locationsService.getAll();
    }

    @PostMapping
    public void save(@RequestBody LocationSaveDTO saveDTO){
        locationsService.save(saveDTO);
    }

}
