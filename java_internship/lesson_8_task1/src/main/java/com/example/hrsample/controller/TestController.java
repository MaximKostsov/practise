package com.example.hrsample.controller;

import com.example.hrsample.model.Locations;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController("/")
public class TestController {
    @GetMapping("/test")
    public Locations getLocationTest(){
        return new Locations();
    }
}
