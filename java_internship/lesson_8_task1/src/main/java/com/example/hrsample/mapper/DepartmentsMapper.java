package com.example.hrsample.mapper;

import com.example.hrsample.model.Departments;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;
@Mapper
@Repository
public interface DepartmentsMapper {

    @Select("select * from departments")
    List<Departments> getAllDepartments();

}
