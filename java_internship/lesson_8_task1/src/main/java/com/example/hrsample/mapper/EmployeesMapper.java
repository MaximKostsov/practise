package com.example.hrsample.mapper;

import com.example.hrsample.model.Employees;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface EmployeesMapper {

//    @Results(value = {
//            @Result(property="job", column = "job_id", one=@One,
//                    typeHandler = JobsTypeHandler.class),
//            @Result(property = "department", column = "department_id", one = @One,
//                    typeHandler = DepartmentsTypeHandler.class)
//    })
    @Select("select * from employees")
    List<Employees> getAllEmployees();
}
