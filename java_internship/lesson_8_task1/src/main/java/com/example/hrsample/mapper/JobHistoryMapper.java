package com.example.hrsample.mapper;

import com.example.hrsample.model.JobHistory;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;
@Mapper
@Repository
public interface JobHistoryMapper {

    @Select("select * from job_history")
    List<JobHistory> getAll();
}
