package com.example.hrsample.mapper;

import com.example.hrsample.model.Jobs;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface JobsMapper {
    @Select("select * from jobs")
    List<Jobs> getAllJobs();
}
