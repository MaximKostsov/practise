package com.example.hrsample.mapper;

import com.example.hrsample.model.Locations;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface LocationsMapper {
    @Select("select * from locations")
    List<Locations> getAll();


    @Insert("INSERT into locations(street_address,city) VALUES(#{l.streetAddress},#{l.city})")
    void save(@Param("l") Locations locations);
}
