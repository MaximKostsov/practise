package com.example.hrsample.model;

import lombok.Data;

@Data
public class Departments {
    private Integer departmentsId;
    private String departmentsName;
    private Integer managerId;
    private Integer locationId;
}
