package com.example.hrsample.model;

import lombok.Data;

@Data
public class Employees {
    private Integer employeeId;
    private String firstName;
    private String lastName;
    private String email;
    private String phoneNumber;
    private String hireDate;
    private Integer jobId;
    private Integer salary;
    private Integer managerId;
    private Integer departamentsId;
}
