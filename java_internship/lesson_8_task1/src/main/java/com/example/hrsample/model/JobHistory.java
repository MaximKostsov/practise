package com.example.hrsample.model;

import lombok.Data;

@Data
public class JobHistory {
    private Integer employeeId;
    private String startDate;
    private String endDate;
    private String jobId;
    private Integer departamentsId;
}
