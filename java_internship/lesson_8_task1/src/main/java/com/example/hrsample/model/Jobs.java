package com.example.hrsample.model;

import lombok.Data;

@Data
public class Jobs {
    private String jobId;
    private String jobTitle;
    private Integer minSalary;
    private Integer maxSalary;
}
