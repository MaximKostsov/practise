package com.example.hrsample.model;

import lombok.Data;

@Data
public class Locations {
    private Integer locationId;
    private String streetAddress;
    private String city;
}
