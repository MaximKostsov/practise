package com.example.hrsample.service;
import com.example.hrsample.DTO.DepartmentsSaveDTO;
import com.example.hrsample.mapper.DepartmentsMapper;
import com.example.hrsample.model.Departments;
import com.example.hrsample.model.Locations;
import com.example.hrsample.service.api.DepartmentsService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;
import java.util.stream.Collectors;
@Service
public class DepartmentsServiceImpl implements DepartmentsService {

    private final ModelMapper mapper = new ModelMapper();

    @Autowired
    private DepartmentsMapper departmentsMapper;

    @Override
    public List<Departments> getAllDepartments() {
       return  departmentsMapper.getAllDepartments();
    }

}

