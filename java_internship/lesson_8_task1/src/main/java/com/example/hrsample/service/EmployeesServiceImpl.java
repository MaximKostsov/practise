package com.example.hrsample.service;

import com.example.hrsample.DTO.EmployeesSaveDTO;
import com.example.hrsample.mapper.EmployeesMapper;
import com.example.hrsample.model.Employees;
import com.example.hrsample.service.api.EmployeesService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeesServiceImpl implements EmployeesService {

    private final ModelMapper mapper = new ModelMapper();

    @Autowired
    private EmployeesMapper employeesMapper;


    @Override
    public List<Employees> getAllEmployees() {
        return employeesMapper.getAllEmployees();
    }
}
