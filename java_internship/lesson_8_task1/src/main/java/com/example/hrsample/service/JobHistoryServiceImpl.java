package com.example.hrsample.service;

import com.example.hrsample.mapper.JobHistoryMapper;
import com.example.hrsample.model.JobHistory;
import com.example.hrsample.service.api.JobHistoryService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class JobHistoryServiceImpl implements JobHistoryService {

    private final ModelMapper mapper = new ModelMapper();
    @Autowired
    private JobHistoryMapper jobHistoryMapper;


    @Override
    public List<JobHistory> getAllJobHistory() {
        return  jobHistoryMapper.getAll();
    }
}
