package com.example.hrsample.service;

import com.example.hrsample.mapper.JobsMapper;
import com.example.hrsample.model.Jobs;
import com.example.hrsample.service.api.JobsService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class JobsServiceImpl implements JobsService {

    private final ModelMapper mapper = new ModelMapper();

    @Autowired
    private JobsMapper jobsMapper;



    @Override
    public List<Jobs> getAllJobs() {
        return jobsMapper.getAllJobs();
    }
}
