package com.example.hrsample.service;

import com.example.hrsample.DTO.LocationSaveDTO;
import com.example.hrsample.mapper.LocationsMapper;
import com.example.hrsample.model.Locations;
import com.example.hrsample.service.api.LocationsService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class LocationsServiceImpl implements LocationsService {

    private final ModelMapper mapper = new ModelMapper();

    @Autowired
    private LocationsMapper locationsMapper;

    @Override
    public List<Locations> getAll() {
        return locationsMapper.getAll();
    }


    @Override
    public void save(LocationSaveDTO locationSaveDTO) {
        Locations locations = mapper.map(locationSaveDTO, Locations.class);
        locationsMapper.save(locations);
    }

}
