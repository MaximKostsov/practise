package com.example.hrsample.service.api;

import com.example.hrsample.DTO.DepartmentsSaveDTO;
import com.example.hrsample.model.Departments;

import java.util.List;

public interface DepartmentsService {

    List<Departments> getAllDepartments();

}
