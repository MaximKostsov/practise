package com.example.hrsample.service.api;

import com.example.hrsample.DTO.EmployeesSaveDTO;
import com.example.hrsample.model.Employees;

import java.util.List;

public interface EmployeesService {
    List<Employees> getAllEmployees();
}
