package com.example.hrsample.service.api;

import com.example.hrsample.DTO.JobHistorySaveDTO;
import com.example.hrsample.model.JobHistory;

import java.util.List;

public interface JobHistoryService {
    List<JobHistory> getAllJobHistory();
}
