package com.example.hrsample.service.api;

import com.example.hrsample.DTO.JobsSaveDTO;
import com.example.hrsample.model.Jobs;

import java.util.List;

public interface JobsService {
    List<Jobs> getAllJobs();
}
