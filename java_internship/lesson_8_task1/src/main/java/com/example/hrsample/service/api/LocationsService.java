package com.example.hrsample.service.api;

import com.example.hrsample.DTO.LocationSaveDTO;
import com.example.hrsample.model.Locations;

import java.util.List;

public interface LocationsService {
    List<Locations> getAll();
    void save(LocationSaveDTO locationSaveDTO);
}
