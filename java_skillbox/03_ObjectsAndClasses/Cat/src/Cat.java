public class Cat
{
    private double originWeight;
    private double weight;
    private static final int EYE = 2;
    private static final double MIN_WEIGHT = 1000.0;
    private static final double MAX_WEIGHT = 9000.0;
    private double  feedEat;
    private static int countCat = 0;
    private int countDeadOrExploded ;
    private Color colorCat;

    public  Cat()
    {
        weight = 1500.0 + 3000.0 * Math.random();
        originWeight = weight;
        feedEat = 0;
        countCat++;
        countDeadOrExploded = 0;
        colorCat = Color.BLACK;
    }

    public  Cat(double weight){
        this();
        this.weight = weight;
        this.originWeight = weight;
    }

    public  Cat(double originWeight,double weight, double feedEat, Color color){
        this();
        this.weight = weight;
        this.feedEat = feedEat;
        this.colorCat = color;
        this.originWeight = originWeight;
    }


    public Color getColorCat()
    {
        return colorCat;
    }

    public void setColorCat(Color color)
    {
        colorCat = color;
    }

    public static int getCountCat()
    {
        return countCat;
    }

    public void meow()
    {
        if(weight < MIN_WEIGHT || weight > MAX_WEIGHT) {
            System.out.println("Кошка мертва (");
        } else {
            weight = weight - 1;
            System.out.println("Meow");
        }

    }

    public void feed(Double amount)
    {
        if(weight < MIN_WEIGHT || weight > MAX_WEIGHT) {
            System.out.println("Кошка мертва (");
        } else {
        weight = weight + amount;
        feedEat +=  amount ;
        }
    }

    public void pee()
    {
        if(weight < MIN_WEIGHT || weight > MAX_WEIGHT) {
            System.out.println("Кошка мертва (");
        } else {
            weight = weight - 50.0;
            System.out.println("Pee");
        }    }

    public void drink(Double amount)
    {
        if(weight < MIN_WEIGHT || weight > MAX_WEIGHT) {
            System.out.println("Кошка мертва (");
        } else {
        weight = weight + amount;}
    }

    public Double getWeight()
    {
        return weight;
    }

    public Double getOriginWeight()
    {
        return originWeight;
    }

    public Double getFeed()
    {
        return feedEat;
    }

    public String getStatus()
    {
        if (countDeadOrExploded == 0){
            if(weight < MIN_WEIGHT) {
                countCat--;
                countDeadOrExploded++;
                return "Dead";
            }
            else if(weight > MAX_WEIGHT) {
                countCat--;
                countDeadOrExploded++;
                return "Exploded";
            }
            else if(weight > originWeight) {
                return "Sleeping";
            }
            else {
                return "Playing";
            }

        }
        else {
            return "Кошка скончалась";
            }

    }
}