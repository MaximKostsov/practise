import java.util.Scanner;

public class Loader
{
    public static void main(String[] args)
    {
        int deadCount = 0 ;
        int explodedCount = 0 ;
        System.out.println("Введите вес Звезды");
        Scanner scanner = new Scanner(System.in);
        double weight = scanner.nextInt();
        Cat pushok = new Cat();
        Cat barsik = new Cat();
        Cat vesna = new Cat();
        Cat zvezda = new Cat(weight);
        Cat pushinka = new Cat();
        System.out.println(Cat.getCountCat());
        //=====================================
        System.out.println(pushok.getWeight());
        System.out.println(barsik.getWeight());
        System.out.println(vesna.getWeight());
        System.out.println(zvezda.getWeight()+ "вес Звезды");
        System.out.println(pushinka.getWeight());
        //=====================================
        pushinka.feed(1.2);
        zvezda.feed(1.5);
        System.out.println(zvezda.getWeight());
        System.out.println(pushinka.getWeight());
        //=====================================

        int countEating = 0 ;
        while ( explodedCount == 0 ){
            pushinka.feed(1.2);
            countEating++;
            if (pushinka.getStatus().equals("Exploded")) {
                explodedCount++;
                System.out.println(pushinka.getStatus());
                System.out.println("Кошку покормили " + countEating + " раз!");
            }
        }
        //=====================================

        int meowCount = 0 ;

        while ( deadCount == 0 ){
            pushok.meow();
            meowCount++;
            if (pushok.getStatus().equals("Dead")) {
                deadCount++;
                System.out.println(pushok.getStatus());
                System.out.println("Кошку мяукнула " + meowCount + " раз!");
            }
        }
        //========================================
        vesna.pee();
        System.out.println(vesna.getWeight());
        vesna.pee();
        System.out.println(vesna.getWeight());
        //========================================
        barsik.feed(150.0);
        System.out.println(barsik.getWeight());
        System.out.println(barsik.getFeed());
        //========================================
        System.out.println(Cat.getCountCat());
        System.out.println(pushok.getStatus());
        //========================================
        Cat kittenOne = getKitten();
        Cat kittenTwo = getKitten();
        Cat kittenTree = getKitten();
        System.out.println(kittenOne.getWeight());
        System.out.println(kittenTwo.getWeight());
        System.out.println(kittenTree.getWeight());
        System.out.println(Cat.getCountCat());
        //========================================
        System.out.println(kittenOne.getColorCat());
        kittenOne.setColorCat(Color.RED);
        System.out.println(kittenOne.getColorCat());
        //========================================
       // Cat clonVesna = new Cat(vesna.getOriginWeight(),vesna.getWeight(),vesna.getFeed(),vesna.getColorCat());
        Cat clonVesna = copy(vesna);
        System.out.println(clonVesna.getOriginWeight());
        System.out.println(vesna.getOriginWeight());
        System.out.println(clonVesna.getWeight());
        System.out.println(vesna.getWeight());
        System.out.println(clonVesna.getFeed());
        System.out.println(vesna.getFeed());
        System.out.println(clonVesna.getColorCat());
        System.out.println(vesna.getColorCat());
        System.out.println(Cat.getCountCat());
        System.out.println(zvezda.getWeight()+ "вес Звезды");
    }
    private static Cat getKitten(){
        return new Cat(1100.0);
        }
    public static Cat copy(Cat cat){
        double weight = cat.getWeight();
        double feedEat = cat.getFeed();
        Color color = cat.getColorCat();
        double originWeight= cat.getOriginWeight();
        return new Cat(originWeight, weight, feedEat, color);
    }

}



