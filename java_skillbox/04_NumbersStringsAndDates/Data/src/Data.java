import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.Locale;


public class Data {
    public static void main(String[] args){
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("dd.MM.yyyy", new Locale("ru"));
        DateTimeFormatter dayFormat = DateTimeFormatter.ofPattern("EE", new Locale("ru"));
        LocalDate birthday = LocalDate.of(1994, Month.NOVEMBER, 24);
        LocalDate today = LocalDate.now();
        int countYears = 0;
        
        while (today.isAfter(birthday)){
            System.out.println(countYears + " - " +  dateFormat.format(birthday) + " - " + dayFormat.format(birthday));
            birthday = birthday.plusYears(1);
            countYears++;
        }}
}