public class GeometryCalculator {

    public static double circleSquare;
    public static double sphereVolume;
    public static double triangleSquare;
    public static boolean triangleRightAngled;

    public static double getCircleSquare(double radius) {
        circleSquare = Math.PI * Math.pow(Math.abs(radius), 2);
        return circleSquare;
    }

    // метод должен использовать абсолютное значение radius
    public static double getSphereVolume(double radius) {
        sphereVolume = (4.0/3.0) * (Math.PI * Math.pow(Math.abs(radius), 3));
        return sphereVolume;
    }

    public static boolean isTriangleRightAngled(double a, double b, double c) {
        triangleRightAngled = false ;
        triangleRightAngled = a + b > c && b + c > a && c + a > b;
        return triangleRightAngled;
    }

    // перед расчетом площади рекомендуется проверить возможен ли такой треугольник
    // методом isTriangleRightAngled, если невозможен вернуть -1.0
    public static double getTriangleSquare(double a, double b, double c) {
        isTriangleRightAngled(a, b, c);
        triangleSquare = -1.0;
        if (triangleRightAngled){
            double halfP = (a + b + c)/2;
            triangleSquare = Math.sqrt(halfP*(halfP-a)*(halfP-b)*(halfP-c));
        }
        return triangleSquare;
    }
}
