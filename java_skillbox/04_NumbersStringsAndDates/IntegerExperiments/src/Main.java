public class Main
{
    public static void main(String[] args)
    {
        Container container = new Container();
        container.count += 7843;
        int firstNumber = 12345;
        int secondNumber = 10;
        int thirdNumber = 5059191;
        System.out.println(sumDigits(firstNumber));
        System.out.println(sumDigits(secondNumber));
        System.out.println(sumDigits(thirdNumber));
    }

    public static int sumDigits(int number)
    {
        int sumResult = 0;
        String stringNumber = Integer.toString(number) ;
        int stringLength = stringNumber.length();

        for (int countIndex = 0; countIndex < stringLength ; countIndex++){
            char somePathOfNumber = stringNumber.charAt(countIndex);
            int valurOfNumber = Character.getNumericValue(somePathOfNumber);
            sumResult += valurOfNumber;
        }
        return sumResult;
    }
}
