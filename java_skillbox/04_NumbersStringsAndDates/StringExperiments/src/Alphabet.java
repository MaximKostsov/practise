public class Alphabet {
    public static void main(String[] args){
        String alphabet = "abcdefghijklmnopqrstuvwxyz";
        int lengthAlphabet = alphabet.length();

        for(int countIndexLetter= 0; countIndexLetter < lengthAlphabet; countIndexLetter++){
            char c = alphabet.charAt(countIndexLetter);
            System.out.println("Символ: " + c + "\t" + "Код: " + (int) c);
        }
    }
}
