
public class Loader {
    public static void main(String[] args) {
        String text = "Вася заработал 5000 рублей, Петя - 7531 рублей, а Маша - 30000 рублей";
        System.out.println(countMoney(text));
    }

    public static int countMoney(String str) {
        String[] friends = str.split(",\\s+");
        int sumFriends = 0;
        for (String friend : friends) {
            sumFriends += Integer.parseInt(friend.replaceAll("[^0-9]", ""));
        }
            return sumFriends;
    }
}