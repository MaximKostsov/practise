import java.util.Scanner;

public class Telephone {
    public static void main(String[] args) {
        System.out.println("Введите номер :");
        Scanner scanner = new Scanner(System.in);
        String telephoneNumber = scanner.nextLine();
        telephone(telephoneNumber);
    }

    public static void telephone(String number) {
        String numberWithoutSimbol = number.replaceAll("[^0-9]", "");

        if(numberWithoutSimbol.length()==11){
            if(numberWithoutSimbol.charAt(0) == '8'){
                String newNumber = numberWithoutSimbol.replace("8","7");
                System.out.println("Все ок поменяли " + newNumber);
                System.out.println(newNumber.replaceAll("(\\d)(\\d{3})(\\d{3})(\\d{2})(\\d)", "+$1 ($2) $3-$4-$5"));
            } else if (numberWithoutSimbol.charAt(0) == '7'){
                System.out.println("Все ок " + numberWithoutSimbol.replaceAll("(\\d)(\\d{3})(\\d{3})(\\d{2})(\\d)", "+$1 ($2) $3-$4-$5"));
            }else {
                System.out.println("не то ");
            }

        }

    }
}