import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;


public class Hospital {
    public static void main (String[] args) {
        int patients = 30;
        float avgTemperature;
        float sumTemperature = 0;
        int countHelthPatients = 0;
        float minTemperature, maxTemperature ;
        float helthMinTemperature , helthMaxTemperature ;
        float[] temperatures = new float[patients];
        ArrayList<Float> helthTemperatures = new ArrayList<>();
        for(int countPatients = 0;  countPatients  < patients  ; countPatients++) {
            temperatures[countPatients] = (float) (Math.round(((Math.random()*(40.0-32.0)) + 32.0)*100.0) / 100.0 );
            sumTemperature +=  temperatures[countPatients];

            if (temperatures[countPatients] >= 36.0 && temperatures[countPatients] <= 36.9 ) { countHelthPatients++;
                helthTemperatures.add(temperatures[countPatients]);
            }
        }
        Arrays.sort(temperatures);
        System.out.println("Темпиратуры пациентов: " + Arrays.toString(temperatures).replace("[", "").replace("]", ""));
        minTemperature = temperatures[0];
        maxTemperature = temperatures[temperatures.length-1];

        Collections.sort(helthTemperatures);
        helthMinTemperature = helthTemperatures.get(0);
        helthMaxTemperature = helthTemperatures.get(helthTemperatures.size()-1);

        avgTemperature = sumTemperature / (float) patients ;
        System.out.println("Средняя темпиратура: " + (Math.round(avgTemperature*100.0)/100.0));
        System.out.println("Здоровых пациентов: " + countHelthPatients);
        System.out.println("Минимальная темпиратура пациентов: " + minTemperature +
                ", Максимальная темпиратура пациентов: " +  maxTemperature);
        System.out.println("Минимальная темпиратура здоровых пациентов: " + helthMinTemperature +
                ", Максимальная здоровых темпиратура пациентов: " +  helthMaxTemperature);

    }
}
