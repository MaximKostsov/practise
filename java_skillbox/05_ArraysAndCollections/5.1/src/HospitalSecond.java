import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


class HospitalSecond {
    public static void main (String[] args) {
        final int PATIENTS = 30;
        final double MAX_TEMP = 40.0;
        final double MIN_TEMP = 36.0;
        final double MAX_TEMP_HELPH = 36.9;
        final double MIN_TEMP_HELPH = 36.2;
        double avgTemperature;
        double sumTemperature = 0;
        long countHelthPatients;
        double minTemperature, maxTemperature ;
        Optional<Double> helthMinTemperature, helthMaxTemperature;
        ArrayList<Double> scrollTemperatures = new ArrayList<>();
        DecimalFormat tempiratureFormat = new DecimalFormat("#0.00°C");
        for(int countPatients = 0;  countPatients  < PATIENTS  ; countPatients++) {
            double temperature = Math.random()*(MAX_TEMP-MIN_TEMP) +  MIN_TEMP;
            sumTemperature +=  temperature ;
            scrollTemperatures.add(temperature);
        }
        System.out.println("Темпиратуры пациентов: " + scrollTemperatures.toString().replace("[", "").replace("]", ""));
        Collections.sort(scrollTemperatures);
        avgTemperature = sumTemperature / PATIENTS ;
        minTemperature = scrollTemperatures.get(0);
        maxTemperature = scrollTemperatures.get(scrollTemperatures.size()-1);
        countHelthPatients = scrollTemperatures.stream().filter(i -> ( i > MIN_TEMP_HELPH && i <= MAX_TEMP_HELPH )).count();
        List<Double> scrollHelthTemperatures = scrollTemperatures.stream().filter(i -> ( i > MIN_TEMP_HELPH && i <= MAX_TEMP_HELPH )).collect(Collectors.toList());
        helthMinTemperature = scrollHelthTemperatures.stream().min(Double::compareTo);
        helthMaxTemperature = scrollHelthTemperatures.stream().max(Double::compareTo);

        System.out.println("Минимальная темпиратура пациентов: " + tempiratureFormat.format(minTemperature) +
                ", Максимальная темпиратура пациентов: " +  tempiratureFormat.format(maxTemperature));
        System.out.println("Средняя темпиратура: " + tempiratureFormat.format(avgTemperature));
        System.out.println("Здоровых пациентов: " + countHelthPatients);
        System.out.println("Минимальная темпиратура здоровых пациентов: " + tempiratureFormat.format(helthMinTemperature.get()) +
                ", Максимальная здоровых темпиратура пациентов: " +  tempiratureFormat.format(helthMaxTemperature.get()));

    }
}