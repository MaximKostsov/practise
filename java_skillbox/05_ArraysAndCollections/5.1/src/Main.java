import java.util.Arrays;

public class Main {
    public static void main (String[] args) {
        int patients = 30;
        float avgTemperature;
        float sumTemperature = 0;
        int countHelthPatients = 0;
        float minTemperature = 0;
        float maxTemperature = 0;
        float helthMinTemperature = 0;
        float helthMaxTemperature = 0;
        float[] temperatures = new float[patients];
        for(int countPatients = 0;  countPatients  < patients  ; countPatients++) {
            minTemperature = maxTemperature = temperatures[0];
            temperatures[countPatients] = (float) (Math.round(((Math.random()*(40.0-32.0)) + 32.0)*100.0) / 100.0 );
            sumTemperature +=  temperatures[countPatients];

            if (temperatures[countPatients] < minTemperature) { minTemperature = temperatures[countPatients]; }
            if (temperatures[countPatients] > maxTemperature) { maxTemperature = temperatures[countPatients]; }
            if (temperatures[countPatients] >= 36.0 && temperatures[countPatients] <= 36.9 ) { countHelthPatients++;
                if(countHelthPatients == 1) { helthMinTemperature = maxTemperature = temperatures[countPatients];}
                if (temperatures[countPatients] < helthMinTemperature) { helthMinTemperature = temperatures[countPatients]; }
                if (temperatures[countPatients] > helthMaxTemperature) { helthMaxTemperature = temperatures[countPatients]; }
            }
        }
        avgTemperature = sumTemperature / (float) patients ;
        System.out.println("Темпиратуры пациентов: " + Arrays.toString(temperatures).replace("[", "").replace("]", ""));
        System.out.println("Средняя темпиратура: " + (Math.round(avgTemperature*100.0)/100.0));
        System.out.println("Здоровых пациентов: " + countHelthPatients);
        System.out.println("Минимальная темпиратура пациентов: " + minTemperature +
                ", Максимальная темпиратура пациентов: " +  maxTemperature);
        System.out.println("Минимальная темпиратура пациентов: " + helthMinTemperature +
                ", Максимальная темпиратура пациентов: " +  helthMaxTemperature);

    }

}
