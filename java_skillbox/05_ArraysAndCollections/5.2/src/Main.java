import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main (String[] args) {
        String act, nameOfDeal = "";
        int countDeal;
        int countExit = 0;
        int indexchoice = -1;
        ArrayList<String> toDoList = new ArrayList<>();
        toDoList.add("Понять регулярки");
        toDoList.add("Найти мануал");
        toDoList.add("Дописать код");
        toDoList.add("Запушить код");
        while (countExit == 0) {
            Scanner scanner = new Scanner(System.in);
            String dealsList = scanner.nextLine();
            String[] deals = dealsList.split("\\s+");
            act = deals[0];
            if (deals.length > 1){
                if (deals[1].matches("[0-9]+")) {
                    indexchoice = Integer.parseInt(deals[1]);
                    for(int countComent = 2; countComent < deals.length;countComent++)
                        nameOfDeal += deals[countComent] + " ";
                }
                else {
                    for(int countComent = 1; countComent < deals.length;countComent++)
                        nameOfDeal += deals[countComent] + " ";
                }}
            switch (act) {
                case "add" -> {
                    if (indexchoice != -1) {
                        if (indexchoice >= 0 && indexchoice < toDoList.size()) {
                            toDoList.remove(indexchoice - 1);
                            toDoList.add(indexchoice - 1, nameOfDeal);
                        } else {
                            toDoList.add(nameOfDeal);
                        }
                    } else {
                        toDoList.add(nameOfDeal);
                    }
                }
                case "edit" -> {
                    toDoList.remove(indexchoice-1);
                    toDoList.add(indexchoice-1, nameOfDeal);
                }
                case "del" -> {
                    if (indexchoice >= 0 && indexchoice < toDoList.size()) {
                        toDoList.remove(indexchoice - 1);
                    }
                }
                case "list" -> {
                    countDeal = 0;
                    for (String toDo : toDoList) {
                        countDeal++;
                        System.out.println(countDeal + " " + toDo);
                    }
                }
                case "exit" -> countExit++;
            }
        }
    }
}