import java.util.*;


public class Main {
    public static void main (String[] args) {
        TreeMap<String, String> telephoneNumbers = new TreeMap<>();
        telephoneNumbers.put("89998097469", "Максим");
       for (;;) {
           Scanner scanner = new Scanner(System.in);
           String telephonInfo = scanner.nextLine();
           if (telephonInfo.equals("list")) {
               printMap(telephoneNumbers);
           } else if (telephonInfo.matches("^((8|\\+7)[\\- ]?)?(\\(?\\d{3}\\)?[\\- ]?)?[\\d\\- ]{7,10}$")) {
               if (!telephoneNumbers.containsKey(telephonInfo)) {
                   System.out.println("Контакт не найден Имя: ");
                   String numberNew = scanner.nextLine();
                   telephoneNumbers.put(telephonInfo, numberNew);
               }
               System.out.println(telephonInfo + " " + telephoneNumbers.get(telephonInfo));
           }
           else if(telephonInfo.matches("[[А-Я][а-я]-]+")) {
               if (!telephoneNumbers.containsValue(telephonInfo)) {
                   System.out.println("Контакт не найден введите номер телефона: ");
                   String nameNew = scanner.nextLine();
                   telephoneNumbers.put(nameNew, telephonInfo);
               }
               System.out.println(getKey(telephoneNumbers,telephonInfo).toString().replace("[","").replace("]","") + " " + telephonInfo);
           } else {System.out.println("Введены не корректные данные!");}
        }
    }
    private static void printMap(Map<String,String> map){
        for (String key : map.keySet()){
            System.out.println(key + " " + map.get(key));
        }
    }
    public static List<String> getKey(Map<String, String> map, String telephonInfo){
        List<String> keys = new ArrayList<>();
        for (String key : map.keySet()){
            if (map.get(key).equals(telephonInfo)){
                keys.add(key);
            }
        }
        return keys;
}

}
