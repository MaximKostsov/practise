import java.util.*;

public class Main {
    public static  void main(String[] args) {
        ArrayList<String> list = new ArrayList<>();
        String[] letters = {"C", "M", "T", "B", "A", "P", "O", "H", "E", "Y"};
        for (int letterIndex = 0; letterIndex < letters.length; letterIndex++) {
            for (int numberCount = 0; numberCount < 10; numberCount++) {
                for (int countRegion = 1; countRegion < 199; countRegion++) {
                    String letter = letters[letterIndex];
                    String region = String.valueOf(countRegion);
                    if (countRegion < 10) {
                        region = "0" + region;
                    }
                    String number = String.format("%s%d%d%d%s%s%s", letter, numberCount, numberCount, numberCount, letter, letter, region);
                    list.add(number);

                }
            }
        }
        for (int numberCount = 111; numberCount <= 999; numberCount += 111) {
            for (int letterIndexOne = 0; letterIndexOne < letters.length; letterIndexOne++) {
                for (int letterIndexTwo = 0; letterIndexTwo < letters.length; letterIndexTwo++) {
                    for (int letterIndexTree = 0; letterIndexTree < letters.length; letterIndexTree++) {
                        for (int countRegion = 1; countRegion < 199; countRegion++) {
                            String letterOne = letters[letterIndexOne];
                            String letterTwo = letters[letterIndexTwo];
                            String letterThree = letters[letterIndexTree];
                            String region = String.valueOf(countRegion);
                            String numberNumber = String.valueOf(numberCount);
                            if (countRegion < 10) {
                                region = "0" + region;
                            }
                        String number = String.format("%s%s%s%s%s", letterOne, numberNumber, letterTwo, letterThree, region);
                        list.add(number);
                    }}
                }
            }
        }
        Collections.sort(list);
        TreeSet<String> treeSet = new TreeSet<>(list);
        HashSet<String> hashSet = new HashSet<>(list);
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите номер для поиска: ");
        String ourNumber = scanner.nextLine();
        long bruteForceCounter,  binaryCounter, hashSetCounter,  treeSetCounter;
        bruteForceCounter = System.nanoTime();
        if (list.contains(ourNumber)) {
            System.out.println("Поиск перебором: номер найден , поиск занял " + (System.nanoTime() - bruteForceCounter) + " нс");
        } else {
            System.out.println("Поиск перебором: номер не найден , поиск занял " + (System.nanoTime() - bruteForceCounter) + " нс");
        }
        binaryCounter = System.nanoTime();
        if (Collections.binarySearch(list, ourNumber) >= 0) {
            System.out.println("Бинарный поиск: номер найден , поиск занял " + (System.nanoTime() - binaryCounter) + " нс");
        } else {
            System.out.println("Бинарный поиск: номер не найден , поиск занял " + (System.nanoTime() - binaryCounter) + " нс");
        }
        hashSetCounter = System.nanoTime();
        if (treeSet.contains(ourNumber)) {
            System.out.println("Поиск в HashSet: номер найден , поиск занял " + (System.nanoTime() - hashSetCounter) + " нс");
        } else {
            System.out.println("Поиск в HashSet: номер не найден , поиск занял " + (System.nanoTime() - hashSetCounter) + " нс");
        }
        treeSetCounter = System.nanoTime();
        if (hashSet.contains(ourNumber)) {
            System.out.println("Поиск в TreeSet: номер найден , поиск занял " + (System.nanoTime() - treeSetCounter) + " нс");
        } else {
            System.out.println("Поиск в TreeSet: номер не найден , поиск занял " + (System.nanoTime() - treeSetCounter) + " нс");
        }
    }}



