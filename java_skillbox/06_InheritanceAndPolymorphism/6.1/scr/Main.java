

import scores.Card;
import scores.Deposit;

public class Main {
    public static void main (String[] args){
        Card bankCardOne = new Card(23000);
        Card bankCardTwo = new Card(234000);
        Deposit bankDepositOne = new Deposit(12000);
        Deposit bankDepositTwo = new Deposit(132500);
        System.out.println( bankCardOne.send(bankCardTwo,13000));
        System.out.println( bankCardOne.getBalance());
        System.out.println( bankCardTwo.getBalance());
        System.out.println( bankCardOne.toString());
        System.out.println( bankDepositOne.toString());
    }
}
