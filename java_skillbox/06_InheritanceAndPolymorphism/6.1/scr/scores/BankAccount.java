package scores;

public class BankAccount {
    private double balance ;


    public BankAccount(double startBalans){
        putMoney(startBalans);
    }

    protected double getBalance() {
        return balance;
    }

    protected boolean takeOff(double withdrawn)
    {
        if((balance - withdrawn) < 0){
            System.out.println("На счету недостаточно средст!" );
            return false;
        } else {balance -= withdrawn;
            return true;}
    }

    protected void putMoney(double accruedMoney){
        balance += accruedMoney ;
    }


    boolean send(BankAccount receiver, double amount){
        double temporaryBalance = receiver.getBalance();
        if(takeOff(amount)){
        receiver.putMoney(amount);
        return receiver.getBalance() > temporaryBalance;}
        else {
            return false;
        }
    }



}
