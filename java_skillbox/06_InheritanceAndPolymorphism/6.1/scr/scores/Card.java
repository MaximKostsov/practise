package scores;

public class Card extends BankAccount {

    static final double PERCENT = 0.01;

    public Card(double startBalans) {
        super(startBalans);
    }

    @Override
    public boolean takeOff(double withdrawn) {
        super.takeOff(withdrawn+(withdrawn*PERCENT));
        return true;
    }

    @Override
    public void putMoney(double accruedMoney) {
        super.putMoney(accruedMoney);
    }

    @Override
    public double getBalance() {
        return super.getBalance();
    }


    @Override
    public boolean send(BankAccount receiver, double amount) {
        return super.send(receiver, amount);
    }

    @Override
    public String toString() {
        return "Карта: " + "баланс карты= " + getBalance()
                + "р., процентная ставка по снятию= " + PERCENT*100 + "%";
    }
}
