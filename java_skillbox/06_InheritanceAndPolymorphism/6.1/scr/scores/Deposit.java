package scores;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class Deposit extends BankAccount {

    private LocalDate dateOfEntry;

    public Deposit(double startBalans) {
        super(startBalans);
        setDate(LocalDate.now());
    }

    protected void setDate(LocalDate newDate){
        dateOfEntry = newDate;
    }

    public boolean DifferenceDate()
    {
        LocalDate dateNow = LocalDate.now();
        return !dateNow.isBefore(dateOfEntry.plusMonths(1));
    }

    @Override
    public boolean takeOff(double withdrawn) {

        if (DifferenceDate()){
            super.takeOff(withdrawn);
            return true;
        }else { System.out.println("C момента последнего пополнения не прошел месяц!" );
        return false;}

    }

    @Override
    public void putMoney(double accruedMoney)
    {
        setDate(LocalDate.now());
        super.putMoney(accruedMoney);
    }

    @Override
    public double getBalance() {
        return super.getBalance();
    }

    @Override
    public boolean send(BankAccount receiver, double amount) {
        return super.send(receiver, amount);
    }

    @Override
    public String toString() {
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("dd.MM.yyyy", new Locale("ru"));
        return "Депозит:" +
                " дата пополнения: " + dateFormat.format(dateOfEntry) +
                " баланс= " + getBalance() + "р.";
    }
}
