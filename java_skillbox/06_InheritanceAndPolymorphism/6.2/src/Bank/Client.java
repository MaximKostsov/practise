package Bank;

abstract class Client {

    private double balance ;
    private int checkingAccount ;

    public void putMoney(double accruedMoney ){
        balance += accruedMoney ;
    }

    public boolean takeOff(double withdrawn){
        if((balance - withdrawn) < 0){
            System.out.println("На счету недостаточно средст!" );
            return false;
        } else {balance -= withdrawn;
            return true;}
    }

    protected void setCheckingAccount(int numberAccount){
        checkingAccount = numberAccount;
    }
    public int getCheckingAccount() {
        return checkingAccount;
    }
    public double getBalance() {
        return balance;
    }


}
