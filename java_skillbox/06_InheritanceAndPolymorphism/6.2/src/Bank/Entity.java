package Bank;

public class Entity extends Client {

    static final double PERCENT = 0.01;

    public Entity(double startBalans, int checkingAccount) {
        setCheckingAccount(checkingAccount);
        putMoney(startBalans);
    }

    @Override
    public boolean takeOff(double withdrawn) {
         if((balance - withdrawn) < 0){
            System.out.println("На счету недостаточно средст!" );
            return false;
        } else {super.takeOff(withdrawn+(withdrawn*PERCENT));
            return true;}

    }

    @Override
    public String toString() {
        return "Счет: №" + getCheckingAccount() + " баланс счета= " + getBalance()
                + "р., снятие с комиссией " + PERCENT*100 + "%" ;
    }
}
