package Bank;

public class Individual extends Client {
    public Individual(double startBalans, int checkingAccount){
        setCheckingAccount(checkingAccount);
        putMoney(startBalans);
    }

    @Override
    public String toString() {
        return "Счет: №" + getCheckingAccount()+ " баланс счета= " + getBalance()
                + "р., пополнение и снятие происходит без комиссии";

    }
}
