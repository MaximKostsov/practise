package Bank;

public class Sp extends Client {

    static final double PERCENT_LESS = 0.01;

    static final double PERCENT_MORE = 0.005;

    public Sp(double startBalans, int checkingAccount) {
        setCheckingAccount(checkingAccount);
        putMoney(startBalans);
    }
    @Override
    public void putMoney(double accruedMoney) {
        if(accruedMoney<1000) {
            super.putMoney(accruedMoney - (accruedMoney * PERCENT_LESS));
        }else {
            super.putMoney(accruedMoney - (accruedMoney * PERCENT_MORE));
        }
    }


    @Override
    public String toString() {
        return "Счет: №" + getCheckingAccount() + " баланс счета= " + getBalance()
                + "р., пополнение с комиссией " + PERCENT_LESS*100 + "%, если сумма меньше 1000 рублей. " +
                "\nИ с комиссией " + PERCENT_MORE*100 + "% если сумма больше либо равна 1000 рублей"  ;
    }
}
