import Bank.Entity;
import Bank.Individual;
import Bank.Sp;

public class Main {
    public static void main (String[] args){
        Individual bankOne = new Individual(23000,1);
        Entity bankTwo = new Entity(234000,2);
        Sp bankThree = new Sp(12000,3);

        bankOne.takeOff(1000);
        bankTwo.takeOff(1000);
        bankThree.takeOff(13000);
        bankOne.putMoney(100);
        bankTwo.putMoney(100);
        bankThree.putMoney(100);
        System.out.println( bankOne.getBalance() + " " + bankOne.getCheckingAccount());
        System.out.println( bankTwo.getBalance() + " " + bankTwo.getCheckingAccount());
        System.out.println( bankThree.getBalance() + " " + bankThree.getCheckingAccount());
        System.out.println(bankOne.toString());
        System.out.println( bankTwo.toString());
        System.out.println(bankThree.toString());


    }
}
