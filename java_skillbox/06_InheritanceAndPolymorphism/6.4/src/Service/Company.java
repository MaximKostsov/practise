package Service;

import java.util.ArrayList;
import java.util.List;


public class Company  {
    private final int income;
    public List<Employee> listSalary;


    public Company(int incomeSet) {
        listSalary = new ArrayList<>();
        income = incomeSet;
    }

    public void hire(Employee staff, Company name){
        staff.setName(name);
        listSalary.add(staff);

    }

    public void hireALL(List<Employee> staff){
        for (Employee employee : staff){
            employee.setName(this);
            listSalary.add(employee);
        }
    }

    public void fireStuff(Employee staff){
       if (listSalary.contains(staff)){
           listSalary.remove(staff);
           staff.setName(null);
       } else {System.out.println("Сотрудника нет в списке ");}
    }

    double getIncome(){
        return income;
    }

    public List<Employee> getListSalary() {
        return listSalary;
    }

    public ArrayList<Employee> getTopSalaryStaff(int count){
        return new ArrayList<>(listSalary.subList((listSalary.size()) - count, listSalary.size()));
    }

   public ArrayList<Employee> getLowestSalaryStaff(int count){
        return new ArrayList<>(listSalary.subList(0, count));
   }

}
