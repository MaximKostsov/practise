package Service;

import Service.Company;

public interface Employee
{
   int getMonthSalary();
   void setName(Company company);
}
