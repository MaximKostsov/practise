package Service;

import Service.Company;

public class Manager extends Staff {

    private static final double  PERCENT = 0.05 ;
    private static final int MIN_TEMP = 115000;
    private static final int MAX_TEMP = 140000 ;
    private final int randomValue = (int) (((Math.random() * (MAX_TEMP - MIN_TEMP)) + MIN_TEMP)*PERCENT);


    public Manager() {
        getMonthSalary();
    }

    @Override
    public int getMonthSalary() {
        return Staff.SALARY + randomValue;
    }

    @Override
    public void setName(Company name) {
        companyName = name;
    }


}
