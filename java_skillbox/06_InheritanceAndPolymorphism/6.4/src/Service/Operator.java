package Service;

import Service.Company;

public class Operator extends Staff {

    @Override
    public int getMonthSalary() {
        return Staff.SALARY;
    }

    @Override
    public void setName(Company name) {
        companyName = name;
    }

}
