package Service;

import Service.Company;
import Service.Staff;

public class TopManager extends Staff {
    private static final double  PERCENT = 1.5;
    private static final int  INCAM_COMPANY = 10000000;
    public TopManager() {
     }
    @Override
    public int getMonthSalary() {
        if (companyName.getIncome() > INCAM_COMPANY)
        {return (int) (SALARY + SALARY * PERCENT);}
        else { return SALARY;}
    }
    @Override
    public void setName(Company nowCompany) {
        companyName = nowCompany;
    }

}


