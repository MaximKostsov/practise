import core.Line;
import core.Station;
import junit.framework.TestCase;

import java.util.*;

public class RouteCalculatorTest extends TestCase {
    List<Station> route;
    RouteCalculator routeCalculator;
    StationIndex testStationIndex;
    Station stationOneLineOne, stationThreeLineTwo, stationTwoLineOne, stationThreeLineOne, stationOneLineTwo,
            stationTwoLineTwo, stationTwoLineThree, stationThreeLineThree, stationOneLineThree;

    @Override
    protected void setUp() throws Exception {
        testStationIndex = new StationIndex();

        Line line1 = new Line(1, "Первая");
        Station one = new Station("1", line1);
        Station two = new Station("2", line1);
        line1.addStation(one);
        line1.addStation(two);
        Line line2 = new Line(2, "Вторая");
        Station three = new Station("3", line2);
        Station four = new Station("4", line2);
        line2.addStation(three);
        line2.addStation(four);
        Line line3 = new Line(3, "Третья");
        Station five = new Station("5", line3);
        Station six = new Station("6", line3);
        line3.addStation(five);
        line3.addStation(six);

        route = new ArrayList<>();
        route.add(one);
        route.add(two);
        route.add(three);
        route.add(four);
        route.add(five);
        route.add(six);


        Line lineFirst = new Line(1, "First");
        Line lineSecond = new Line(2, "Second");
        Line lineThird = new Line(3, "Third");
        testStationIndex.addLine(lineFirst);
        testStationIndex.addLine(lineSecond);
        testStationIndex.addLine(lineThird);

        stationOneLineOne = new Station("stationOneLineOne", lineFirst);
        stationTwoLineOne = new Station("stationTwoLineOne", lineFirst);
        stationThreeLineOne = new Station("stationThreeLineOne", lineFirst);

        lineFirst.addStation(stationOneLineOne);
        lineFirst.addStation(stationTwoLineOne);
        lineFirst.addStation(stationThreeLineOne);

        stationOneLineTwo = new Station("stationOneLineTwo", lineSecond);
        stationTwoLineTwo = new Station("stationTwoLineTwo", lineSecond);
        stationThreeLineTwo = new Station("stationThreeLineTwo", lineSecond);

        lineSecond.addStation(stationOneLineTwo);
        lineSecond.addStation(stationTwoLineTwo);
        lineSecond.addStation(stationThreeLineTwo);

        stationOneLineThree = new Station("stationOneLineThree", lineThird);
        stationTwoLineThree = new Station("stationTwoLineThree", lineThird);
        stationThreeLineThree = new Station("stationThreeLineOne", lineThird);

        lineThird.addStation(stationOneLineThree);
        lineThird.addStation(stationTwoLineThree);
        lineThird.addStation(stationThreeLineThree);


        testStationIndex.addStation(stationOneLineOne);
        testStationIndex.addStation(stationTwoLineOne);
        testStationIndex.addStation(stationThreeLineOne);
        testStationIndex.addStation(stationOneLineTwo);
        testStationIndex.addStation(stationTwoLineTwo);
        testStationIndex.addStation(stationThreeLineTwo);
        testStationIndex.addStation(stationOneLineThree);
        testStationIndex.addStation(stationTwoLineThree);
        testStationIndex.addStation(stationThreeLineThree);

        List<Station> connection1to2 = new ArrayList<>();
        connection1to2.add(stationTwoLineOne);
        connection1to2.add(stationTwoLineTwo);
        testStationIndex.addConnection(connection1to2);
        List<Station> connection2to3 = new ArrayList<>();
        connection2to3.add(stationThreeLineTwo);
        connection2to3.add(stationThreeLineThree);
        testStationIndex.addConnection(connection2to3);
        routeCalculator = new RouteCalculator(testStationIndex);


    }

    public void testCalculateDuration() {
        double actual = RouteCalculator.calculateDuration(route);
        double expected = 14.5;
        assertEquals(actual, expected);
    }

    public void testZeroStation() {
        List<Station> actual = routeCalculator.getShortestRoute(stationOneLineOne, stationOneLineOne);
        List<Station> expected = new ArrayList<>();
        expected.add(stationOneLineOne);

        assertEquals(actual, expected);
    }


    public void testOneLine() {
        List<Station> actual = routeCalculator.getShortestRoute(stationOneLineOne, stationThreeLineOne);
        List<Station> expected = new ArrayList<>();
        expected.add(stationOneLineOne);
        expected.add(stationTwoLineOne);
        expected.add(stationThreeLineOne);
        assertEquals(actual, expected);
    }

    public void testTwoLine() {
        List<Station> actual = routeCalculator.getShortestRoute(stationOneLineOne, stationThreeLineTwo);
        List<Station> expected = new ArrayList<>();
        expected.add(stationOneLineOne);
        expected.add(stationTwoLineOne);
        expected.add(stationTwoLineTwo);
        expected.add(stationThreeLineTwo);
        assertEquals(actual, expected);
    }

    public void testTreeLine() {
        List<Station> actual = routeCalculator.getShortestRoute(stationOneLineOne, stationOneLineThree);
        List<Station> expected = new ArrayList<>();
        expected.add(stationOneLineOne);
        expected.add(stationTwoLineOne);
        expected.add(stationTwoLineTwo);
        expected.add(stationThreeLineTwo);
        expected.add(stationThreeLineThree);
        expected.add(stationTwoLineThree);
        expected.add(stationOneLineThree);
        assertEquals(actual, expected);
    }

}