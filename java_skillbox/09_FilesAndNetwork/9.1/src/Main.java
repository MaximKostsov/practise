import org.apache.commons.io.FileUtils;

import java.io.File;
import java.text.DecimalFormat;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        for(;;){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите путь :");
        String path = scanner.nextLine();
       try {
        File folder = new File(path);
        long size = FileUtils.sizeOfDirectory(folder);

        String[]units = new String[]{ "B", "KB", "MB", "GB", "TB" };
        int unitIndex = (int) (Math.log10(size)/3);
        double unitValue = 1 << (unitIndex *  10);

        String readableSize = new DecimalFormat("#,##0.#")
                .format(size/unitValue) + " "
                + units[unitIndex];
        System.out.println(readableSize);
    }catch (Exception ex){
            ex.printStackTrace();
       }}}
}
