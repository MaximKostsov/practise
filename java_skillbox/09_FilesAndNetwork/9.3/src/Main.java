import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;

public class Main {

    private static String PATH_CSV = "file/movementList.csv";
    private static String dateFormat = "dd.MM.yyyy";
    private static String leftInfo, rightInfo;


    public static void main(String[]args){
        ArrayList<Movements> movements = loadMovementsFromFile();
        System.out.println("Сумма доходов:");
        movements.stream().map(Movements::getComing).reduce(Double::sum).ifPresent(System.out::println);
        System.out.println("Сумма расходов:");
        movements.stream().map(Movements::getConsumption).reduce(Double::sum).ifPresent(System.out::println);
        printMap(listBreakdownOfCosts(breakdownOfCosts(movements) ,movements));

    }


    private static ArrayList<Movements> loadMovementsFromFile()
    {
        ArrayList<Movements> movements = new ArrayList<>();
        try
        {   int countLine = 0;
            List<String> lines = Files.readAllLines(Paths.get(PATH_CSV));
            for(String line : lines)
            {   countLine ++;
            if(countLine != 1){
                String[] fragments = line.split(",", 8);

                if(fragments.length != 8) {
                    System.out.println("Wrong line: " + line);
                    continue;
                }
                movements.add(new Movements.Builder()
                        .nowAccountType(fragments[0])
                        .nowAccountNumber(fragments[1])
                        .nowCurrency(fragments[2])
                        .nowDateOfOperation((new SimpleDateFormat(dateFormat)).parse(fragments[3]))
                        .nowReferenceWiring(fragments[4])
                        .nowOperationDescription(fragments[5])
                        .nowComing(Double.parseDouble(fragments[6].replace(",", ".").replace("\"","")))
                        .nowConsumption(Double.parseDouble(fragments[7].replace(",", ".").replace("\"","")))
                        .build()
                );
            }
            }

        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        return movements;
    }
    private static TreeSet<String>  breakdownOfCosts (ArrayList<Movements> movements){
        TreeSet<String> companingMap = new TreeSet<>();
        for (Movements test : movements ){
            if (test.getOperationDescription().contains("/"))
                {leftInfo = test.getOperationDescription().substring(0,test.getOperationDescription().lastIndexOf("/") + 1);}
                else {leftInfo = test.getOperationDescription().substring(0,test.getOperationDescription().lastIndexOf("\\") + 1);}
                rightInfo = test.getOperationDescription().substring(test.getOperationDescription().lastIndexOf("          "));
            companingMap.add(test.getOperationDescription().replace(leftInfo,"").replace(rightInfo,"").trim());
        }
                return companingMap;
    }

    private static Map<String, Double>  listBreakdownOfCosts (TreeSet<String> company,  ArrayList<Movements> movements){
        Map<String, Double> listOfCosts = new HashMap<>();
        for (String testMap :  company){
            listOfCosts.put(testMap,0.0);
            for (Movements test : movements ){
                if (test.getOperationDescription().contains(testMap))
                {
                    listOfCosts.put(testMap, listOfCosts.get(testMap) + test.getConsumption());
                }
        }
        }
        return listOfCosts;
    }

    private static void printMap (Map<String, Double> map){
        for(String key : map.keySet()){
            System.out.println(key + " " + map.get(key) + " р.");
        }
    }

}
