import java.util.Date;

public class Movements {

    private String accountType, accountNumber, currency, referenceWiring, operationDescription;
    private Double  coming, consumption;
    private Date dateOfOperation;

    public static class Builder{
        private Movements newMovements;
        public Builder(){
            newMovements = new Movements();
        }

        public Builder nowAccountType(String accountType){
            newMovements.accountType = accountType;
            return this;
        }

        public Builder nowAccountNumber(String accountNumber){
            newMovements.accountNumber = accountNumber;
            return this;
        }

        public Builder nowCurrency(String currency){
            newMovements.currency = currency;
            return this;
        }

        public Builder nowDateOfOperation(Date dateOfOperation){
            newMovements.dateOfOperation = dateOfOperation;
            return this;
        }

        public Builder nowReferenceWiring(String referenceWiring){
            newMovements.referenceWiring = referenceWiring;
            return this;
        }

        public Builder nowOperationDescription(String operationDescription){
            newMovements.operationDescription = operationDescription;
            return this;
        }
        public Builder nowComing(Double coming){
            newMovements.coming = coming;
            return this;
        }
        public Builder nowConsumption(Double consumption){
            newMovements.consumption = consumption;
            return this;
        }

        public Movements build(){
            return newMovements;
        }
    }

    public Double getConsumption() {
        return consumption;
    }

    public Double getComing() {
        return coming;
    }

    public String getOperationDescription() {
        return operationDescription;
    }
}
