import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.*;
import java.net.URL;


public class Main {
    public static final String FOLDERPATH = "images";
public static void main(String[] args) throws IOException {
    Document doc = Jsoup.connect(" https://lenta.ru").get();
    Elements link = doc.select("img");
    for (Element src : link) {
        String absHref = src.attr("abs:src");
        if (absHref.matches(".*\\.(jpg|png)(:|$).*"))
        {
        getImages(absHref);
    }else {System.out.println("Не картинка " + absHref);}
    }


}

    private static void getImages(String absHref) throws IOException {
        int startName = absHref.lastIndexOf("/");

        if (startName == absHref.length()) {
            absHref = absHref.substring(1, startName);
        }

        startName = absHref.lastIndexOf("/");
        String name = absHref.substring(startName + 1);
        System.out.println(name);

        URL url = new URL(absHref);
        InputStream in = url.openStream();

        OutputStream out = new BufferedOutputStream(new FileOutputStream(FOLDERPATH + "\\" + name));

        for (int pick; (pick = in.read()) != -1;) {
            out.write(pick);
        }
        out.close();
        in.close();
    }
}
