import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;


public class Main {
    public static void main(String[] args) throws IOException {
        Document doc = Jsoup.connect(" https://www.moscowmap.ru/metro.html#lines").maxBodySize(0).get();
        Elements aContent = doc.select("div");
        JSONArray jsLines = new JSONArray();
        JSONObject jsStations = new JSONObject();
        JSONObject jsMap = new JSONObject();
        for (Element inputElement : aContent) {
            if (inputElement.className().equals("js-metro-stations t-metrostation-list-table")){
                List<String> listStation = new ArrayList<>() ;
                Elements names =  inputElement.select("span");
                String nomberOfline = inputElement.dataset()
                        .values()
                        .toString()
                        .replace("[", "")
                        .replace("]", "") ;

                for (Element name : names){
                if ( name.className().equals("name")){
                    listStation.add(name.text());
                System.out.println(nomberOfline + " " + name.text() );}
                }
                jsStations.put(nomberOfline, listStation);
            }

        }

        for (Element inputElement : aContent) {
            if (inputElement.className().equals("js-toggle-depend s-depend-control-single"))
            {
                Elements names = inputElement.select("span");
                for (Element name : names){
                    JSONObject jr = new JSONObject();
                    String numberOfline = inputElement.dataset()
                            .values()
                            .toString()
                            .replace("{'toggle-slide':'lines-", "")
                            .replace("'}", "")
                            .replace("[", "")
                            .replace("]", "") ;
                    System.out.println(numberOfline + " " + name.text());
                    jr.put("number", numberOfline);
                    jr.put("name", name.text());
                    jsLines.add(jr);
                }
            }
        }
        jsMap.put("stations",jsStations);
        jsMap.put("lines", jsLines);
        System.out.println( jsMap);

        try{
            FileWriter file = new FileWriter("map.JSON");
            file.write(jsMap.toJSONString());
            file.flush();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        try
        {
            JSONParser parser = new JSONParser();
            JSONObject jsonData = (JSONObject) parser.parse(getJsonFile());

            JSONArray linesArray = (JSONArray) jsonData.get("lines");
            System.out.println(linesArray);

            JSONObject stationsObject = (JSONObject) jsonData.get("stations");
            for (Object key : stationsObject.keySet()){
                JSONArray linesArrayStations = (JSONArray) stationsObject.get(key);
                System.out.println("Ветка " + key + " имеет " + (long) linesArrayStations.size() + " станций"); //setkey
        }
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }





    }

    private static String getJsonFile()
    {
        StringBuilder builder = new StringBuilder();
        try {
            List<String> lines = Files.readAllLines(Paths.get("map.JSON"));
            lines.forEach(line -> builder.append(line));
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        return builder.toString();
    }


}



