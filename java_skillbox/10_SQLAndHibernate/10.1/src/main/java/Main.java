import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class Main {
    public static void main(String[] args) {
        String url = "jdbc:mysql://localhost:3306/skillbox?serverTimezone=Europe/Moscow&useSSL=false";
        String user = "root";
        String pass = "testtest";

        try {
            Connection connection = DriverManager.getConnection(url, user, pass);

            Statement statement = connection.createStatement();

            statement.execute("Update Courses SET name='Веб-разработчик с нуля до PRO' WHERE id = 1");

            ResultSet resultSet = statement.executeQuery("SELECT name, COUNT(subscription_date), (MONTH(MAX(subscription_date)) - MONTH(MIN(subscription_date))) as MonthCount , subscription_date FROM Courses JOIN Subscriptions ON Courses.id = Subscriptions.course_id GROUP BY Subscriptions.course_id");
            while(resultSet.next())
            {
                String courseName = resultSet.getString("name");
                String courseCount = resultSet.getString("COUNT(subscription_date)");
                String courseDate = resultSet.getString("MonthCount");
                System.out.println(courseName + " всреднем за месяц продано " + (Double.parseDouble(courseCount)/Double.parseDouble(courseDate)) + " курсов");
            }
            resultSet.close();
            statement.close();
            connection.close();

        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
}
