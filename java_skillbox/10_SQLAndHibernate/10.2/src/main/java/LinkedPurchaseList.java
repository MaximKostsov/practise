import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "LinkedPurchaseList")
public class LinkedPurchaseList implements Serializable {

        @EmbeddedId
        private LinkedPurchaseListId linkedPurchaseListId;

        @ManyToOne
        @JoinColumn(name = "studentId", insertable = false, updatable = false)
        @MapsId("studentIdLp")
        private Student student;

        @ManyToOne
        @JoinColumn(name = "courseId",insertable = false, updatable = false)
        @MapsId("coursesIdLp")
        private Course course;

        private int price;

        @Column(name = "subscription_date")
        private Date subscriptionDate;

    public LinkedPurchaseListId getLinkedPurchaseListId() {
        return linkedPurchaseListId;
    }

    public void setLinkedPurchaseListId(LinkedPurchaseListId linkedPurchaseListId) {
        this.linkedPurchaseListId = linkedPurchaseListId;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Date getSubscriptionDate() {
        return subscriptionDate;
    }

    public void setSubscriptionDate(Date subscriptionDate) {
        this.subscriptionDate = subscriptionDate;
    }
}
