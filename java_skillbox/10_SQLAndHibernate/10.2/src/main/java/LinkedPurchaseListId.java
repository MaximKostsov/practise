import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class LinkedPurchaseListId implements Serializable {


    @Column(name = "studentId")
    private int studentIdLp;

    @Column(name = "courseId")
    private int coursesIdLp;

    public LinkedPurchaseListId(int studentIdLp, int coursesIdLp) {
        this.studentIdLp = studentIdLp;
        this.coursesIdLp = coursesIdLp;
    }

    public LinkedPurchaseListId() {
    }

    public int getStudentIdLp() {
        return studentIdLp;
    }

    public void setStudentIdLp(int studentIdLp) {
        this.studentIdLp = studentIdLp;
    }

    public int getCoursesIdLp() {
        return coursesIdLp;
    }

    public void setCoursesIdLp(int coursesIdLp) {
        this.coursesIdLp = coursesIdLp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LinkedPurchaseListId that = (LinkedPurchaseListId) o;
        return studentIdLp == that.studentIdLp &&
                coursesIdLp == that.coursesIdLp;
    }

    @Override
    public int hashCode() {
        return Objects.hash(studentIdLp, coursesIdLp);
    }


}
