import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                .configure("hibernate.cfg.xml").build();
        Metadata metadata = new MetadataSources(registry).getMetadataBuilder().build();
        SessionFactory sessionFactory = metadata.getSessionFactoryBuilder().build();

        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

//        Course course = session.get(Course.class, 1);
//        System.out.println(course.getTeacher().getName());
//        System.out.println(course.getStudents().size());
//        List<Student> studentList = course.getStudents();
//        studentList.forEach(s -> System.out.println(s.getName()));
//
//        Student student = session.get(Student.class, 1);
//        System.out.println(student.getCoursess().size());
//        List<Course> coursesList =  student.getCoursess();
//        coursesList.forEach(c -> System.out.println(c.getName()));
////===============================================================================
//        CriteriaBuilder builder = session.getCriteriaBuilder();
//        CriteriaQuery<Course> query = builder.createQuery(Course.class);
//        Root<Course> root = query.from(Course.class);
//        query.select(root).where(builder.greaterThan(root.<Integer>get("price"), 100000))
//        .orderBy(builder.desc(root.get("price")));
//        List<Course> courseList = session.createQuery(query).setMaxResults(5).getResultList();
//        for (Course course1 : courseList){
//            System.out.println(course1.getName() + " - " + course1.getPrice());
//        }
////===============================================================================
//        String hql = "From " + Course.class.getSimpleName() + " Where price > 120000";
//        List<Course> coursesList1 = session.createQuery(hql).getResultList();
//        for (Course course2 : coursesList1){
//            System.out.println(course2.getName() + " - " + course2.getPrice());
//        }
//        System.out.println( coursesList1.size());
//
//        PurchaseList purchaseList = session.get(PurchaseList.class, new PurchaseListId ("Амбражевич Порфирий", "Веб-разработчик c 0 до PRO"));
//        System.out.println(purchaseList.getPrice());
//        System.out.println(purchaseList.getPurchaseListId().getCourseName());
//        System.out.println(purchaseList.getSubscriptionDate());

        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<PurchaseList> criteriaBuilderQuery = criteriaBuilder.createQuery(PurchaseList.class);
        Root<PurchaseList> rootEntry = criteriaBuilderQuery.from(PurchaseList.class);
        CriteriaQuery<PurchaseList> all = criteriaBuilderQuery.select(rootEntry);

        TypedQuery<PurchaseList> allQuery = session.createQuery(all);
        List<PurchaseList> purchaseLists =allQuery.getResultList();
        for(PurchaseList pl: purchaseLists){
            LinkedPurchaseListId id = new LinkedPurchaseListId();
            id.setCoursesIdLp(pl.getCourse().getId());
            id.setStudentIdLp(pl.getStudent().getId());


            LinkedPurchaseList linkedPurchaseList = new LinkedPurchaseList();
            linkedPurchaseList.setLinkedPurchaseListId(id);
            linkedPurchaseList.setStudent(pl.getStudent());
            linkedPurchaseList.setCourse(pl.getCourse());
            linkedPurchaseList.setPrice(pl.getPrice());
            linkedPurchaseList.setSubscriptionDate(pl.getSubscriptionDate());
            session.save(linkedPurchaseList);
        }


        transaction.commit();
        sessionFactory.close();
    }
}