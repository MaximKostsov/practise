import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "PurchaseList")
public class PurchaseList implements Serializable {


    @EmbeddedId
    private  PurchaseListId  purchaseListId;

    private int price;

    @Column(name = "subscription_date")
    private Date subscriptionDate;

    @ManyToOne
    @JoinColumn(name="student_name", insertable = false, updatable = false, referencedColumnName="name")
    private Student student;

    @ManyToOne
    @JoinColumn(name="course_name", insertable = false, updatable = false, referencedColumnName="name")
    private Course course;

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Date getSubscriptionDate() {
        return subscriptionDate;
    }

    public void setSubscriptionDate(Date subscriptionDate) {
        this.subscriptionDate = subscriptionDate;
    }

    public PurchaseListId getPurchaseListId() {
        return purchaseListId;
    }

    public void setPurchaseListId(PurchaseListId purchaseListId) {
        this.purchaseListId = purchaseListId;
    }


}
