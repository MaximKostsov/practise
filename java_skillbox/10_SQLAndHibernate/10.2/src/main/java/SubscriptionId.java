import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class SubscriptionId implements Serializable {


    @Column(name = "student_id")
    private int studentId;
    @Column(name = "course_id")
    private int coursesId;

    public SubscriptionId() {
    }


    public SubscriptionId(int studentId, int coursesId) {
        this.studentId = studentId;
        this.coursesId = coursesId;
    }



    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public int getCoursesId() {
        return coursesId;
    }

    public void setCoursesId(int coursesId) {
        this.coursesId = coursesId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SubscriptionId that = (SubscriptionId) o;
        return studentId == that.studentId &&
                coursesId == that.coursesId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(studentId, coursesId);
    }
}
