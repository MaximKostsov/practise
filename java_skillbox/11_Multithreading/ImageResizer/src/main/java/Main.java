import java.io.File;

public class Main
{
    private static int newWidth = 300;
    private static final double processors = Runtime.getRuntime().availableProcessors();
    private static int delta;

    public static void main(String[] args)
    {
        String srcFolder = "img";
        String dstFolder = "dst";
        File srcDir = new File(srcFolder);
        long start = System.currentTimeMillis();
        File[] files = srcDir.listFiles();
        double middle = files.length / processors  ;
        int delta = (int)Math.ceil(middle) ;
        for(int count = 0; count < files.length ; count +=  delta){
            File[] filesX = new File[delta];
            System.arraycopy(files, count, filesX, 0, filesX.length);
            new Thread( new ImagerResizer(filesX, dstFolder, start)).start();
        }

        System.out.println(processors);

    }

}
