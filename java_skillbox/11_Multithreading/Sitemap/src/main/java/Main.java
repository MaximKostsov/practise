import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.concurrent.ForkJoinPool;

public class Main {
    public static void main(String[] args) throws IOException {
        String SITE = "https://lenta.ru/";
        SitemapNode sitemap = new SitemapNode(SITE);
        new ForkJoinPool().invoke(new SitemapNodeRecursiveAction(sitemap));

        FileOutputStream stream = new FileOutputStream("sitemap.txt");
        String result = createSitemapString(sitemap, 0);
        stream.write(result.getBytes());
        stream.flush();
        stream.close();
    }

    public static String createSitemapString(SitemapNode node, int depth) {
        String tabs = String.join("", Collections.nCopies(depth, "\t"));
        StringBuilder result = new StringBuilder(tabs + node.getUrl());
        node.getChildren().forEach(child -> result.append("\n").append(createSitemapString(child, depth + 1)));
        return result.toString();
    }
}
