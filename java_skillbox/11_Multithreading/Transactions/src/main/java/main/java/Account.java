package main.java;

public class Account implements Comparable<Account> {
    private long money;
    private String accNumber;


    private boolean status;

    public Account(String accNumber, long money) {
        this.money = money;
        this.accNumber = accNumber;
        this.status = true;
    }

    boolean send(Account receiver, long amount) {
        double temporaryBalance = receiver.getMoney();
        if (takeOff(amount)) {
            receiver.putMoney(amount);
            return receiver.getMoney() > temporaryBalance;
        } else {
            return false;
        }
    }

    protected boolean putMoney(long accruedMoney) {
        if (!status) {
            System.out.println("Счет получателя заблокирован");
            return false;
        } else {
            money += accruedMoney;
            return true;
        }


    }

    protected boolean takeOff(long withdrawn) {
        if ((money - withdrawn) < 0) {
            System.out.println("На счету недостаточно средст!");
            return false;
        } else if (!status) {
            System.out.println("Счет отправителя заблокирован");
            return false;
        } else {
            money -= withdrawn;
            return true;
        }
    }


    public long getMoney() {
        return money;
    }

    public void setMoney(long money) {
        this.money = money;
    }

    public String getAccNumber() {
        return accNumber;
    }

    public void setAccNumber(String accNumber) {
        this.accNumber = accNumber;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public void inactiveAccount() {
        setStatus(false);
    }

    @Override
    public int compareTo(Account o) {
        return this.getAccNumber().compareTo(o.getAccNumber());
    }
}

