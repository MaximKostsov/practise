package main.java;

import java.util.HashMap;
import java.util.Random;

public class Bank {
    public HashMap<String, Account> accounts;
    private final Random random = new Random();

    public synchronized boolean isFraud(String fromAccountNum, String toAccountNum, long amount)
            throws InterruptedException {

        Thread.sleep(1000);
        return random.nextBoolean();
    }

    public Bank(HashMap<String, Account> accounts) {
        this.accounts = accounts;
    }

    /**
     * TODO: реализовать метод. Метод переводит деньги между счетами.
     * Если сумма транзакции > 50000, то после совершения транзакции,
     * она отправляется на проверку Службе Безопасности – вызывается
     * метод isFraud. Если возвращается true, то делается блокировка
     * счетов (как – на ваше усмотрение)
     */
    public void transfer(String fromAccountNum, String toAccountNum, long amount) throws InterruptedException {

        Account fromAccount = accounts.get(fromAccountNum);
        Account toAccount = accounts.get(toAccountNum);
        Account lowSyncAccount;
        Account topSyncAccount;

        if (fromAccount.compareTo(toAccount) > 0) {
            lowSyncAccount = toAccount;
            topSyncAccount = fromAccount;
        } else {
            lowSyncAccount = fromAccount;
            topSyncAccount = toAccount;
        }
        synchronized (lowSyncAccount) {
            synchronized (topSyncAccount) {

                if (amount > 50000) {
                    if (!isFraud(fromAccountNum, toAccountNum, amount)) {
                        if (fromAccount.takeOff(amount)) {
                            toAccount.putMoney(amount);
                        }
                    } else {
                        System.out.println("Аккаунт" + toAccount.getAccNumber() + " и "
                                + fromAccount.getAccNumber() + "заблокирован");
                        fromAccount.inactiveAccount();
                        toAccount.inactiveAccount();
                    }
                } else {
                    if (fromAccount.takeOff(amount)) {
                        toAccount.putMoney(amount);
                    }
                }
            }
        }


    }

    public long getBalance(String accountNum) {

        return accounts.get(accountNum).getMoney();
    }
}
