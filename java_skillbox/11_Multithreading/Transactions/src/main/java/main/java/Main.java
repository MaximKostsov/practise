package main.java;

import java.util.HashMap;

public class Main {

    private static Bank vtb;

    public static void main(String[] args) throws InterruptedException {
        HashMap<String, Account> test = new HashMap<>();
        test.put("ПервыйB", new Account("ПервыйA", 500000));
        test.put("ВторойB", new Account("ВторойA", 500000));
        test.put("ТретийB", new Account("ТретийA", 15000));
        test.put("ЧетвертыйB", new Account("ЧетвертыйA", 500));
        test.put("ПятыйB", new Account("ПятыйA", 5000));
        vtb = new Bank(test);
        bankOparation("ПервыйB", "ВторойB", 50);

        for (int i = 0; i < 12; i++) {
            new Thread(() -> {
                try {
                    for (int j = 0; j<2000;j++){
                    bankOparation("ПервыйB", "ВторойB", 50);
                    bankOparation("ВторойB", "ПервыйB", 50);
                        bankOparation("ТретийB", "ВторойB", 50); }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }).start();
        }
//        System.out.println(test.get("ПервыйB").getMoney());

    }


    private static void bankOparation(String fist, String second, int money) throws InterruptedException {
        System.out.println("C " + fist + " на " + second);

        vtb.transfer(fist, second, money);
        System.out.println("Cтало на первом " + vtb.getBalance(fist) + " стало на втором " + vtb.getBalance(second));
    }
}
