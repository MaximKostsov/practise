package com.example.todolist.controller;

import com.example.todolist.dto.ToDoDTO;
import com.example.todolist.service.api.ToDoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/todo")

public class ToDoController {
    @Autowired
    private ToDoService toDoService;


    @GetMapping
    public List<ToDoDTO> getAll() {
        return toDoService.getAll();
    }

    @PostMapping("/save")
    public void save(@RequestBody ToDoDTO toDoDTO) {
        toDoService.save(toDoDTO);
    }


    @PostMapping("/delete")
    public void delete(@RequestBody ToDoDTO toDoDTO) {
        toDoService.delete(toDoDTO);
    }

    @PostMapping("/update")
    public void update(@RequestBody ToDoDTO toDoDTO) {
        toDoService.update(toDoDTO);
    }


}