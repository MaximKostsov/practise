package com.example.todolist.dto;


import lombok.Data;


@Data
public class ToDoDTO {
    private Integer todoId;
    private String name;
    private String date;
}
