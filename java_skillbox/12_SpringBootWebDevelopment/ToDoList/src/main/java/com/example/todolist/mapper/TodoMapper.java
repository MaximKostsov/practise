package com.example.todolist.mapper;


import com.example.todolist.model.ToDo;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface TodoMapper {
    @Insert("insert into todo (name, date) " +
            "values (#{t.name}, #{t.date})")
    void save(@Param("t") ToDo toDo);

    @Select("select * from todo")
    List<ToDo> getAll();

    @Delete("delete from todo where todo_id = #{todoId}")
    void delete(ToDo toDo);

    @Update("update todo set name = #{e.name}, date =  #{e.date} where todo_id = #{e.todoId}")
    void update(@Param("e") ToDo toDo);
}
