package com.example.todolist.model;

import lombok.Data;

@Data
public class ToDo {
    private Integer todoId;
    private String name;
    private String date;
}
