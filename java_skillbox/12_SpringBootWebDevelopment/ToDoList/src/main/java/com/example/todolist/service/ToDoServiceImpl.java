package com.example.todolist.service;


import com.example.todolist.dto.ToDoDTO;

import com.example.todolist.mapper.TodoMapper;

import com.example.todolist.model.ToDo;
import com.example.todolist.service.api.ToDoService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ToDoServiceImpl implements ToDoService {
    @Autowired
    private TodoMapper todoMapper;

    private final ModelMapper mapper = new ModelMapper();

    @Override
    public void save(ToDoDTO toDoDTO) {
        ToDo toDo = mapper.map(toDoDTO, ToDo.class);
        todoMapper.save(toDo);
    }

    @Override
    public List<ToDoDTO> getAll() {
        return todoMapper.getAll().stream()
                .map(toDo -> mapper.map(toDo, ToDoDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public void delete(ToDoDTO toDoDTO) {
        ToDo toDo = mapper.map(toDoDTO, ToDo.class);
        todoMapper.delete(toDo);
    }

    @Override
    public void update(ToDoDTO toDoDTO) {
        ToDo toDo = mapper.map(toDoDTO, ToDo.class);
        todoMapper.update(toDo);
    }
}
