package com.example.todolist.service.api;

import com.example.todolist.dto.ToDoDTO;

import java.util.List;

public interface ToDoService {
    void save(ToDoDTO toDoDTO);

    List<ToDoDTO> getAll();

    void delete(ToDoDTO toDoDTO);

    void update(ToDoDTO toDoDTO);

}
