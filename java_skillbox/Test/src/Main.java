import com.skillbox.airport.Airport;
import com.skillbox.airport.Flight;
import com.skillbox.airport.Terminal;


import java.util.Date;
import java.util.List;


public class Main {
    public static void main(String[] args){
        Airport airport = Airport.getInstance();
        List<Terminal> terminalOne = airport.getTerminals();
        Date dateAfter = new Date(System.currentTimeMillis());
        Date dateBefore = new Date(dateAfter.getTime() + 7200*1000);

        terminalOne.stream().flatMap(f -> f.getFlights().stream())
                .filter(f -> f.getType().equals(Flight.Type.ARRIVAL)
                 && f.getDate().before(dateBefore)
                 && f.getDate().after(dateAfter)).forEach(System.out::println);

        //        terminalOne.forEach(t -> t.getFlights()
//                .stream()
//                .filter(f -> f.getType().equals(Flight.Type.ARRIVAL)
//                        && f.getDate().before(dateBefore)
//                        && f.getDate().after(dateAfter))
//                .forEach(System.out::println));

//         for (Terminal terminal:terminalOne){
//             System.out.println( terminal.getFlights());
//            for(Flight flight:terminal.getFlights()){
//                if (flight.getType().equals(Flight.Type.ARRIVAL)){
//                System.out.println( "---------------");
//                System.out.println( flight.getDate());
//                System.out.println( flight.getType());
//                System.out.println( flight.getCode());
//                System.out.println( "---------------");
//
//            }
//            }
//
//         }
    }
}
