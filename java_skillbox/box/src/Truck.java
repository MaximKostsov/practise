public class Truck {
    public int countTruck ;
    public int countContainer ;
    public int countBox;
    public int haveBox;
    public static final int MAX_CONTAINER_IN_TRUCK = 12;
    public static final int MAX_BOX_IN_CONTAINER = 27;

    public Truck(int box)
    {
       countTruck = 0;
       countContainer = 0;
       countBox = 0;
       haveBox = box;
    }

    public int getCountTruck(){ return  countTruck; }

    public int getCuntContainer(){ return countContainer; }

    public void calculation()
    {
        while(haveBox>0){
            countTruck++;
            System.out.println("Грузовик " + countTruck + " :" );
                for (int countMaxContainer = 0 ; countMaxContainer < MAX_CONTAINER_IN_TRUCK && haveBox >0; countMaxContainer++){
                    countContainer++;
                    System.out.println("\t"+"Контейнер " + countContainer + " :" );
                    for (int countMaxBox = 0;countMaxBox < MAX_BOX_IN_CONTAINER && haveBox >0; countMaxBox++){
                        countBox++;
                        System.out.println("\t"+"\t"+"Ящик " + countBox);
                        haveBox--;
                    }
                }
        }
    }
}
